﻿// @TODO(stdmatt): All this code is totally a mess...
// It's being created adhoc to make the things to compile.
// It need to be refactored soon as possible. Jan 24, 2021
//
#pragma once

// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Time.hpp"

namespace ark { namespace Random {

class RNG;
namespace Export {
    template <typename T>
    T CreateRandom(RNG *rng);

    template <typename T>
    T CreateRandom(RNG *rng, T *max);

    template <typename T>
    T CreateRandom(RNG *rng, T* min, T *max);
}

//
// Enums / Constants / Typedefs
//
ARK_STRICT_CONSTEXPR static size_t RANDOM_SEED = NumericLimits<size_t>::Max;

class RNG
{
public:
    template <typename Type> Type Next()                 { return Next(Type(0), Max<Type>());      }
    template <typename Type> Type Next(Type hi)          { return Next(Type(0), hi);               }
    template <typename Type> Type Next(Type lo, Type hi) { return lo + (rand() % (hi - lo)); }

    explicit
    RNG(size_t const seed)
        : _seed(seed)
    {
        if(_seed == RANDOM_SEED) {
            _seed = Cast<size_t>(::time(nullptr));
        }
    }

private:
    size_t _seed;
};

RNG& GetDefaultRNG();

template <typename Type> Type Next()                 { return GetDefaultRNG().Next(Type(0), Max<Type>()); }
template <typename Type> Type Next(Type hi)          { return GetDefaultRNG().Next(Type(0), hi);          }
template <typename Type> Type Next(Type lo, Type hi) { return GetDefaultRNG().Next(lo, hi);               }


} // namespace Random
} // namespace ark
