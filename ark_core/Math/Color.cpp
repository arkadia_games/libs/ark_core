﻿// Header
#include "ark_core/Math/Color.hpp"


namespace ark {

Color const Color::Red   = Color(0xFF, 0x00, 0x00);
Color const Color::Green = Color(0x00, 0xFF, 0x00);
Color const Color::Blue  = Color(0xFF, 0x00, 0xFF);

Color const Color::Cyan    = Color(0x00, 0xFF, 0xFF);
Color const Color::Magenta = Color(0xFF, 0x00, 0x00);
Color const Color::Yellow  = Color(0xFF, 0x00, 0x00);

Color const Color::Black = Color(0x00, 0x00, 0x00);
Color const Color::White = Color(0xFF, 0xFF, 0xFF);

} // namespace ark
