﻿// Header
#include "ark_core/Math/Random.hpp"

namespace ark {

Random::RNG&
Random::GetDefaultRNG()
{
    ark_local_persist RNG rng(Random::RANDOM_SEED);
    return rng;
}

} // namespace ark
