#pragma once

// Umbrella Header
#include "CodeUtils/ArkFormat.hpp"
#include "CodeUtils/Casts.hpp"
#include "CodeUtils/CLibUtils.hpp"
#include "CodeUtils/Constants.hpp"
#include "CodeUtils/Constexpr.hpp"
#include "CodeUtils/Disallow.hpp"
#include "CodeUtils/FakeKeywords.hpp"
#include "CodeUtils/Inline.hpp"
#include "CodeUtils/NumericLimits.hpp"
#include "CodeUtils/NumericTypes.hpp"
#include "CodeUtils/Singleton.hpp"
#include "CodeUtils/TypeTraits.hpp"

//
//
//
#define ARK_UNUSED(_var_) ((void)(_var_))
#define ARK_AREA(x, y) (x * y)
#define ARK_BYTES_PER_BITS(_bits_count_) (_bits_count_ / 4)
#define ARK_BITS_PER_BYTES(_bytes_count_) (8 * _bytes_count_)

#define ARK_SUPER(T) typedef T super;

#define ARK_SAFE_ASSIGN(_ptr_, _value_) \
    do {                                \
        if(_ptr_) *_ptr_ = _value_;     \
    } while(0);
#define ARK_SAFE_DELETE(_ptr_)      \
    do {                            \
        if(_ptr_) { delete _ptr_; } \
        _ptr_ = nullptr;            \
    } while(0)
#define ARK_SAFE_FREE(_ptr_)    \
    do {                        \
        ark::FreeMemory(_ptr_); \
    } while(0)

namespace ark {

//------------------------------------------------------------------------------
ARK_FORCE_INLINE void
Exit(i32 const error_code = 0)
{
    ::exit(error_code);
}

} // namespace ark
