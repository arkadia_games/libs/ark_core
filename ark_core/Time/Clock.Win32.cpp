// Arkadia
#include "ark_core/Platform/Discovery.hpp"
#if defined(ARK_OS_IS_WINDOWS)
// Header
#include "ark_core/Time/Clock.hpp"
// Arkadia
#include "ark_core/Platform.hpp"

namespace ark {

//
// Globals
//
//------------------------------------------------------------------------------
ark_global_var bool          g_TicksStarted = false;
ark_global_var LARGE_INTEGER g_StartCounter = {};
ark_global_var LARGE_INTEGER g_PerfFreq     = {};


//
// Helper Functions
//
//------------------------------------------------------------------------------
ark_internal_function void
_ark_TicksInit()
{
    if(g_TicksStarted) {
        return;
    }

    g_TicksStarted = true;

    QueryPerformanceCounter  (&g_StartCounter);
    QueryPerformanceFrequency(&g_PerfFreq);
}


//
// Static Functions
//
//------------------------------------------------------------------------------
u32
Clock::GetTicks()
{
    if (!g_TicksStarted) {
        _ark_TicksInit();
    }

    LARGE_INTEGER ticks;
    QueryPerformanceCounter (&ticks);

    return u32(ticks.QuadPart);
}

//------------------------------------------------------------------------------
f32
Clock::DivisorOfTicksPerSeconds()
{
    return f32(g_PerfFreq.QuadPart);
}

} // namespace ark
#endif // defined(ARK_OS_IS_GNU_LINUX)
