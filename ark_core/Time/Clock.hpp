#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"

namespace ark {

class Clock
{
    //
    // Static Methods
    //
public:
    static u32 GetTicks                ();
    static f32 DivisorOfTicksPerSeconds();

    //
    // CTOR / DTOR
    //
public:
    ARK_FORCE_INLINE
    Clock()
        : _last(Clock::GetTicks())
        , _now (Clock::GetTicks())
    {
        // Empty...
    }

    //
    // Public Methods
    //
public:
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void
    Update() {
        _last = _now;
        _now  = Clock::GetTicks();
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE f32
    Elapsed() const
    {
        return f32(_now - _last) / Clock::DivisorOfTicksPerSeconds();
    }

    //
    // iVars
    //
private:
    i32 _last;
    i32 _now;
}; // class Clock

} // namespace ark
