﻿#pragma once
// std
#include <numeric>
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Containers/Array.hpp"

namespace ark {

class FPSCounter
{
    //
    // Enums / Constants / Typedefs
    //
public:
    constexpr static u32 DEFAULT_SAMPLES_COUNT = 60; // 60 samples (1 minute)

    //
    // CTOR / DTOR
    //
public:
    ARK_FORCE_INLINE explicit
    FPSCounter(u32 const samples_count = DEFAULT_SAMPLES_COUNT)
        : _desired_samples_count(samples_count)
    {
        _samples.Reserve(samples_count);
    }

    //
    // Public Methods
    //
public:
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void
    Update(f32 const delta)
    {
        ++_ticks;
        _total_time += delta;

        if(_total_time > 1.0f) {
            _total_time -= 1.0f;
            if(_samples.Count() < _desired_samples_count) {
                _samples.PushBack(_ticks);
            } else {
                _samples[_samples_index] = _ticks;
                _samples_index = (_samples_index + 1) % _samples.Count(); // Makes it behave like a ring buffer.
            }
            _ticks = 0;
            _average_fps = std::accumulate(std::begin(_samples), std::end(_samples), 0.0f);
        }
    }

    //--------------------------------------------------------------------------
    Array<u32> const & GetSamples   () const { return _samples;        };
    u32                GetFPS       () const { return (_samples.IsEmpty()) ? 0 : _samples.Back(); };
    f32                GetAverageFPS() const { return _average_fps;    };

    //
    // iVars
    //
private:
    u32        _ticks       = 0;
    f32        _total_time  = 0.0f;
    f32        _average_fps = 0.0f;
    Array<u32> _samples;
    u32        _samples_index = 0;
    u32        _desired_samples_count = 0;
}; // class Clock

} // namespace ark
