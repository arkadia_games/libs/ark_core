// Arkadia
#include "ark_core/Platform/Discovery.hpp"
#if defined(ARK_OS_IS_GNU_LINUX)
// Header
#include "ark_core/Time/Clock.hpp"
#include "ark_core/Platform.hpp"

namespace ark {


//
// Globals
//
ark_global_var bool            g_TicksStarted;
ark_global_var struct timespec g_StartTimeSpec;
ark_global_var struct timeval  g_StartTimeVal;


//
// Helper Functions
//
//------------------------------------------------------------------------------
// Directly from SDL (src/timer/unix/SDL_systimer.c) :D
ark_internal_function void
ark_TicksInit()
{
    // @todo(stdmatt): Bullet-proof this... Jan 30, 21
    if(g_TicksStarted) {
        return;
    }

    g_TicksStarted = true;
    int result = clock_gettime(CLOCK_MONOTONIC_RAW, &g_StartTimeSpec);
    ARK_ASSERT(result == 0, "Failed to get clock_gettime - Result: {}", result);
}


//
// Static Functions
//
//------------------------------------------------------------------------------
// Directly from SDL (src/timer/unix/SDL_systimer.c) :D
u32
Clock::GetTicks()
{
    // @todo(stdmatt): Bullet-proof this... Jan 30, 21
    if (!g_TicksStarted) {
        ark_TicksInit();
    }

    struct timespec now = {};
    clock_gettime(CLOCK_MONOTONIC_RAW, &now);
    u32 const ticks = (u32)((now.tv_sec - g_StartTimeSpec.tv_sec) * 1000 + (now.tv_nsec - g_StartTimeSpec.tv_nsec) / 1000000);

    return ticks;
}

//------------------------------------------------------------------------------
f32
Clock::DivisorOfTicksPerSeconds()
{
    return 1000.f;
}

} // namespace ark
#endif // defined(ARK_OS_IS_GNU_LINUX)
