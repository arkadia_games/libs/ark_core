#pragma once

#include "ark_core/Platform/Discovery.hpp"
#if ARK_OS_IS_GNU_LINUX
    #include "ark_core/Gfx/Window/WindowEvents.GnuLinux.hpp"
#elif ARK_OS_IS_WINDOWS
    #include "ark_core/Gfx/Window/WindowEvents.Win32.hpp"
#elif ARK_OS_IS_DOS
    #include "ark_core/Gfx/Window/WindowEvents.DOS.hpp"
#endif
// Arkadia
#include "ark_core/Input/InputCommon.hpp"


namespace ark { namespace Gfx {

struct WindowEvent
{
    enum class Type {
        Invalid,

        // Window
        Create,
        Destroy,
        Move,
        Resize,

        // Input
        KeyDown,
        KeyUp,

        // System
        UnderlyingSystem
    } type;

    //
    // Window
    struct Create {
        u32 width;
        u32 height;
    };

    struct Resize {
        u32 new_width;
        u32 new_height;
    };

    struct Move {
        u32 new_x;
        u32 new_y;
    };

    struct UnderlyingSystem {
        WindowUnderlyingSystemEvent_t data;
    };

    union {
        Create           create_data;
        Resize           resize_data;
        Move             move_data;
        KeyboardEvent    key_data;
        UnderlyingSystem underlying_system_data;
    };
}; // Event

} // namespace Gfx
} // namespace ark
