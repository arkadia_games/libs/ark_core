﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Debug.hpp"
#include "ark_core/Math.hpp"

namespace ark { namespace Gfx {


//-----------------------------------------------------------------------------
struct OffscreenBuffer {
    void *memory; // Order BB GG RR XX, 0xXXRRGGBB
    u32   width;
    u32   height;
    u32   pitch;
    u32   bytes_per_pixel;
}; // OffscreenBuffer

//-----------------------------------------------------------------------------
ARK_INLINE void
PutPixel(OffscreenBuffer *buffer, u32 const x, u32 const y, u32 const &color)
{
    ARK_ASSERT(x < buffer->width,  "Invalid PutPixel at Pos:({},{}) - Buffer Size: ({},{}", x, y, buffer->width, buffer->height);
    ARK_ASSERT(y < buffer->height, "Invalid PutPixel at Pos:({},{}) - Buffer Size: ({},{}", x, y, buffer->width, buffer->height);

    u32 const index = y * buffer->width + x;
    ARK_ASSERT(index < buffer->width * buffer->height, "Write out of memory...");

    u32      *pixel = &Cast<u32*>(buffer->memory)[index];
    *pixel = color;
}

//-----------------------------------------------------------------------------
ARK_INLINE void
PutPixel8(OffscreenBuffer *buffer, u32 const x, u32 const y, u8 const &color)
{
    u32 const index = y * buffer->width + x;
    u8 *pixel = &Cast<u8*>(buffer->memory)[index];
    *pixel = color;
}

} // namespace Gfx
} // namespace ark
