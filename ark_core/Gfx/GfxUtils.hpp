﻿#pragma once

#include "ark_core/Math.hpp"

namespace ark { namespace Gfx {

//-----------------------------------------------------------------------------
Vec2_i32 GetDesktopSize();

} // namespace Gfx
} // namespace ark
