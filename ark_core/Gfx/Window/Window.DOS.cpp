﻿// #include "ark_core/Platform/Discovery.hpp"
// #if defined(ARK_OS_IS_DOS)
// // Header
// #include "ark_core/Gfx/Window/Window.hpp"
// // Arkadia
// #include "ark_core/CodeUtils.hpp"
// #include "ark_core/Math.hpp"
// #include "ark_core/String.hpp"
// #include "ark_core/CMD.hpp"
// #include "ark_core/ArkMain.hpp"
// #include "ark_core/Platform.hpp"

// #define ark_DOS_AllocMemory   ark::AllocMemory
// #define ark_DOS_ReleaseMemory ark::FreeMemory

// // @TODO(stdmatt): We need to handle dos resolutions....

// //
// // Types Definitions
// //
// struct ark_DOS_OffscreenBuffer
//     : public ark::Gfx::OffscreenBuffer
// {
// };

// struct ark::Gfx::Window::_InternalWindow {
//     ark_DOS_OffscreenBuffer *offscreen_buffer;

//     Event            *curr_event_weak_ptr;
//     ark::Gfx::Window *owner_weak_ptr;

//     Rect_i32 client_rect;
//     bool     wants_to_close;
// }; // struct Window::_InternalWindow
// typedef ark::Gfx::Window::_InternalWindow ark_DOS_Window;

// //
// // Helper Functions
// //
// //------------------------------------------------------------------------------
// ark_internal_function ark::String
// ark_DOS_GetDefaultCaption()
// {
//     // @todo(stdmatt): Change it... Jan 29, 21
//     return ark::String("Default Caption");
// }

// //------------------------------------------------------------------------------
// ark_internal_function ARK_INLINE void
// ark_DOS_ResizeScreenBuffer(ark_DOS_Window *window, u32 const new_width, u32 const new_height)
// {
//     ark_DOS_OffscreenBuffer *buffer = window->offscreen_buffer;
//     buffer->width  = new_width;
//     buffer->height = new_height;

//     //
//     // Handle Memory.
//     //
//     size_t const pitch_size  = buffer->bytes_per_pixel * buffer->width;
//     size_t const memory_size = buffer->bytes_per_pixel * buffer->width * buffer->height;

//     ark_DOS_ReleaseMemory(buffer->memory);
//     buffer->memory = ark_DOS_AllocMemory(memory_size);
//     buffer->pitch  = pitch_size;
// }

// u8 dos_memory[320 * 200] = {};
// constexpr u32 VGA_MEM_ADDR = 0xa0000;

// //------------------------------------------------------------------------------
// ark_internal_function ARK_INLINE void
// ark_DOS_FlushBuffer(ark_DOS_Window *window)
// {
//     // @TODO(stdmatt): Clean this up... Jan 30, 21
//     u32 const buffer_w    = window->offscreen_buffer->width;
//     u32 const buffer_h    = window->offscreen_buffer->height;
//     u32 const buffer_area = ark::Area(buffer_w, buffer_h);

//     // dosmemput(window->offscreen_buffer->memory, buffer_area, 0xa0000);

//     u8  *memory = Cast<u8*>(window->offscreen_buffer->memory);
//     u8  *dos    = dos_memory;

//     for(u32 y = 0; y < buffer_h; ++y) {
//         for(u32 x = 0; x < buffer_w; ++x) {
//             u8 *val = &memory[buffer_w * y + x];
//             *dos = *val;
//             dos++;
//         }

//         dos += 320 - buffer_w;
//     }

//     dosmemput(dos_memory, 320 * 200, VGA_MEM_ADDR);
// }

// //------------------------------------------------------------------------------
// ark_internal_function ARK_INLINE
// ark::Rect_i32
// ark_DOS_GetClientRect(ark_DOS_Window *window)
// {
//     // @todo(stdmatt): Implement....
//     return ark::Rect_i32(0, 0, 320, 200);
// }

// //
// // Windows Messages Handlers
// //
// //------------------------------------------------------------------------------
// ark_internal_function void
// ark_DOS_OnWindowCreate(ark_DOS_Window *window)
// {
//     window->client_rect = ark_DOS_GetClientRect(window);
//     if(!window->curr_event_weak_ptr) {
//         return;
//     }

//     window->curr_event_weak_ptr->type = ark::Gfx::Window::Event::Type::Create;
//     ark::Gfx::Window::Event::Create &data = window->curr_event_weak_ptr->create_data;

//     data.width  = window->client_rect.w;
//     data.height = window->client_rect.h;
// }

// //------------------------------------------------------------------------------
// ark_internal_function void
// ark_DOS_OnWindowSize(ark_DOS_Window *window)
// {
//     window->client_rect = ark_DOS_GetClientRect(window);
//     if(!window->curr_event_weak_ptr) {
//         return;
//     }

//     window->curr_event_weak_ptr->type = ark::Gfx::Window::Event::Type::Resize;
//     ark::Gfx::Window::Event::Resize &data = window->curr_event_weak_ptr->resize_data;

//     data.new_width  = window->client_rect.w;
//     data.new_height = window->client_rect.h;
// }

// //------------------------------------------------------------------------------
// ark_internal_function void
// ark_DOS_OnWindowPaint(ark_DOS_Window *window)
// {
//     ark_DOS_FlushBuffer(window);
// }

// //------------------------------------------------------------------------------
// ark_internal_function void
// ark_DOS_OnWindowClose(ark_DOS_Window *window)
// {
//     window->wants_to_close = true;
// }

// //
// //
// //
// //------------------------------------------------------------------------------
// ark_internal_function ark_DOS_Window*
// DOS_CreateWindow(ark::Gfx::Window::CreateOptions const &options)
// {
//     ark_DOS_Window *window = new ark_DOS_Window();

//     window->offscreen_buffer = new ark_DOS_OffscreenBuffer();
//     window->offscreen_buffer->bytes_per_pixel = 1;// @TODO(stdmatt): Remove magic number Jan 29, 21

//     return window;
// }

// //------------------------------------------------------------------------------
// ark_internal_function void
// ark_DOS_DestroyWindow(ark_DOS_Window *&window)
// {
//     // @TODO(stdmatt): Destroy the DOS shit... Jan 29, 21
//     delete window->offscreen_buffer;
//     window->offscreen_buffer = nullptr;

//     delete window;
//     window = nullptr;
// }


// //
// // Factory
// //
// //------------------------------------------------------------------------------
// ark::Gfx::Window *
// ark::Gfx::Window::Create(ark::Gfx::Window::CreateOptions const &create_options)
// {
//     // Dos Graphics
//     ark::DOS::GraphicsInit(ark::DOS::GraphicsMode::VGA_256_COLOR_MODE);
//     // Internal Window & Window
//     ark_DOS_Window   *dos_window = DOS_CreateWindow(create_options);
//     ark::Gfx::Window *window     = new Window();
//     // Set the pointers
//     dos_window->owner_weak_ptr = window;
//     window->_internal_window   = dos_window;

//     return window;
// }

// //------------------------------------------------------------------------------
// void
// ark::Gfx::Window::Destroy(ark::Gfx::Window *&window)
// {
//     // Internal Window
//     ark_DOS_DestroyWindow(window->_internal_window);
//     // Window
//     delete window;
//     window = nullptr;
//     // DOS Graphics
//     ark::DOS::GraphicsShutdown();
// }

// //------------------------------------------------------------------------------
// ark::Gfx::OffscreenBuffer*
// ark::Gfx::Window::GetScreenBuffer()
// {
//     return _internal_window->offscreen_buffer;
// }

// //------------------------------------------------------------------------------
// void
// ark::Gfx::Window::SetScreenBufferSize(u32 const width, u32 const height)
// {
//     ark_DOS_ResizeScreenBuffer(_internal_window, width, height);
// }

// //------------------------------------------------------------------------------
// u32           ark::Gfx::Window::GetWidth    () const { return _internal_window->client_rect.w;   }
// u32           ark::Gfx::Window::GetHeight   () const { return _internal_window->client_rect.h;   }
// ark::Rect_i32 ark::Gfx::Window::ClientRect  () const { return _internal_window->client_rect;     }
// bool          ark::Gfx::Window::WantsToClose() const { return _internal_window->wants_to_close;  }

// //------------------------------------------------------------------------------
// void
// ark::Gfx::Window::SetTitle(String const &str)
// {
//     // Empty...
// }


// //------------------------------------------------------------------------------
// bool __hack_for_first_time = true;
// bool
// ark::Gfx::Window::HandleEvents(Event *event)
// {
//     event->type = Event::Type::Invalid;
//     _internal_window->curr_event_weak_ptr = event;
//     if(__hack_for_first_time) {
//         __hack_for_first_time = false;
//         ark_DOS_OnWindowCreate(_internal_window);
//         return true;
//     }
//     return false;
// }


// //------------------------------------------------------------------------------
// void
// ark::Gfx::Window::FlushBuffers()
// {
//     ark_DOS_FlushBuffer(_internal_window);
// }

// //------------------------------------------------------------------------------
// void
// ark::Gfx::Window::Show()
// {
// //    ShowWindow(_internal_window->window_handle, SW_SHOW);
// }

// //------------------------------------------------------------------------------
// void
// ark::Gfx::Window::Hide()
// {
//     // ShowWindow(_internal_window->window_handle, SW_HIDE);
// }


// //
// // Entry Point
// //
// int main(int argc, char const * argv[])
// {
//     ark::CMD::Set(argc, argv);
//     ark_main(ark::CMD::Get());
// }


// #endif // defined(ARK_OS_IS_WINDOWS)
