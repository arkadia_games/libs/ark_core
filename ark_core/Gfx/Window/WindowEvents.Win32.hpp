#pragma once
// Arkadia
#include "ark_core/Platform/Discovery.hpp"
#if ARK_OS_IS_WINDOWS
#include "ark_core/Platform/PlatformIncludes.hpp"

namespace ark { namespace Gfx { namespace Win32 {

/// @brief
///   Encapsulate the raw even from window of Win32 platform.
///   This is used on the Window::HandleEvents to capture things
///   that we need, but isn't cross-platform enough to be put into ark.
struct WindowUnderlyingSystemEvent {
    HWND   window_handle;
    UINT   msg_type;
    WPARAM w_param;
    LPARAM l_param;
};

} // namespace Win32

using WindowUnderlyingSystemEvent_t = Win32::WindowUnderlyingSystemEvent;

} // namespace Gfx
} // namespace ark

#endif // ARK_OS_IS_WINDOWS
