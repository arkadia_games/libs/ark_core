#pragma once

#include "ark_core/Platform/Discovery.hpp"
#if ARK_OS_IS_WINDOWS
// Arkadia
#include "ark_core/Platform/PlatformIncludes.hpp"
#include "ark_core/Gfx/GfxIncludes.hpp"
#include "ark_core/Gfx/Window.hpp"
#include "ark_core/Gfx/WindowEvents.hpp"
#include "ark_core/Gfx/OffscreenBuffer.hpp"

namespace ark { namespace Gfx {

class Win32_Window :
    Window
{
    friend class Window;

    //
    // CTOR / STOR
    //
public:
    ~Win32_Window() = default;

    //
    // Window Interface
    //
public:
    void            * GetNativeHandle     () const override;
    u32               GetWidth            () const override;
    u32               GetHeight           () const override;
    Rect_i32          GetClientRect       () const override;
    Vec2_i32          GetClientSize       () const override;
    String            GetTitle            () const override;
    bool              WantsToClose        () const override;
    Vec2_i32          GetMousePosition    () const override;

    void SetTitle(String const &str)                    override;
    void SetSize (u32    const width, u32 const height) override;

    void Show() override;
    void Hide() override;

    bool HandleEvents        (WindowEvent *event, bool const dispatch_underlying_system_event = false) override;
    bool HandleEventsBlocking(WindowEvent *event, bool const dispatch_underlying_system_event = false) override;

    void CreateScreenBuffer(
        u32 const width,
        u32 const height,
        u32 const bpp
    ) override;

    OffscreenBuffer* GetCurrentScreenBuffer() override;
    void             FlushBuffers          () override;


    //
    // Helper Methods
    //
private:
    void InitWithOptions(Window::CreateOptions const &options);
    void FlushBufferInternal(HDC const hdc);
    void DestroyInternal();
    //
    // Win32 Event Handlers
    //
private:
    friend LRESULT CALLBACK Win32_WindowProcedure(
        HWND   const window_handle,
        UINT   const msg_type,
        WPARAM const w_param,
        LPARAM const l_param);

    void OnWindowCreate ();
    void OnWindowPaint  ();
    void OnWindowResize ();
    void OnWindowClose  ();
    void OnWindowDestroy();

    //
    // Internal Types
    //
private:
    struct Win32_OffscreenBuffer
        : public OffscreenBuffer
    {
        BITMAPINFO bitmap_info;
    };

    //
    // iVars
    //
private:
    // X11 Things
    HWND                   _window_handle                    = nullptr;
    Win32_OffscreenBuffer *_screen_buffer                    = nullptr;
    WindowEvent           *_curr_event_weak_ptr              = nullptr;
    bool                   _wants_to_close                   = false;
    bool                   _dispatch_underlying_system_event = false;
}; // class Win32_Window


} // namespace Gfx
} // namespace ark
#endif // ARK_OS_IS_WINDOWS
