// Header
#include "ark_core/Gfx/Window/X11_Window.hpp"
#if ARK_OS_IS_GNU_LINUX
// Arkadia
#include "ark_core/Memory.hpp"

namespace ark { namespace Gfx {

// @todo(stdmatt): For all Getters: Check if is better to cache those values? May 1, 21
// @todo(stdmatt): For all x11 calls, check the return values!! May 1, 21

// Returns the picture format for ARGB.
// This method is originally from chrome/common/x11_util.cc.
ark_internal_function XRenderPictFormat*
GetRenderARGB32Format(Display* dpy) {
    static XRenderPictFormat* pictformat = nullptr;
    if (pictformat)
        return pictformat;
    // First look for a 32-bit format which ignores the alpha value.
    XRenderPictFormat templ;
    templ.depth = 32;
    templ.type = PictTypeDirect;
    templ.direct.red = 16;
    templ.direct.green = 8;
    templ.direct.blue = 0;
    templ.direct.redMask = 0xff;
    templ.direct.greenMask = 0xff;
    templ.direct.blueMask = 0xff;
    templ.direct.alphaMask = 0;
    static const unsigned long kMask =
                                   PictFormatType | PictFormatDepth |
                                   PictFormatRed | PictFormatRedMask |
                                   PictFormatGreen | PictFormatGreenMask |
                                   PictFormatBlue | PictFormatBlueMask |
                                   PictFormatAlphaMask;
    pictformat = XRenderFindFormat(dpy, kMask, &templ, 0 /* first result */);
    if (!pictformat) {
        // Not all X servers support xRGB32 formats. However, the XRender spec
        // says that they must support an ARGB32 format, so we can always return
        // that.
        pictformat = XRenderFindStandardFormat(dpy, PictStandardARGB32);
    }
    return pictformat;
}


//
// Factory
//
//------------------------------------------------------------------------------
Window*
Window::Create(CreateOptions const &create_options)
{
    X11_Window *window = new X11_Window();
    window->InitWithOptions(create_options);
    return window;
}

//------------------------------------------------------------------------------
void Window::Destroy(Window *&window)
{
    ARK_SAFE_FREE  (Cast<X11_Window*>(window)->_screen_buf->memory);
    ARK_SAFE_FREE  (Cast<X11_Window*>(window)->_screen_buf);
    ARK_SAFE_DELETE(window);
    // @todo(stdmatt): Destroy the X11 resourcew - May 2, 21
}

//------------------------------------------------------------------------------
Window::CreateOptions
Window::GetDefaultCreateOptions()
{

}


//
// Window Interface
//
//------------------------------------------------------------------------------
void *
X11_Window::GetNativeHandle() const
{
    return Cast<void*>(this); // @todo(stdmatt): Which would be the native handle for X11? May 1, 21
}

//------------------------------------------------------------------------------
u32
X11_Window::GetWidth() const
{
}

//------------------------------------------------------------------------------
u32
X11_Window::GetHeight() const
{
    XWindowAttributes _attr;
    XGetWindowAttributes(_display,_window, &_attr);
    return _attr.width;
}

//------------------------------------------------------------------------------
Rect_i32
X11_Window::GetClientRect() const
{
    XWindowAttributes _attr;
    XGetWindowAttributes(_display,_window, &_attr);
    return Rect_i32(_attr.x, _attr.y, _attr.width, _attr.height);
}

//------------------------------------------------------------------------------
Vec2_i32
X11_Window::GetClientSize() const
{
    XWindowAttributes _attr;
    XGetWindowAttributes(_display,_window, &_attr);
    return Vec2_i32(_attr.width, _attr.height);
}


//------------------------------------------------------------------------------
String
X11_Window::GetTitle() const
{
    char *_windowname;
    if(XFetchName(_display, _window, &_windowname) != 0) {
        if(*_windowname) {
            ark::String ret(*_windowname);
            XFree(_windowname);
        }
    }
    return "";
}

//------------------------------------------------------------------------------
bool
X11_Window::WantsToClose() const
{
    return _wants_to_close;
}

//------------------------------------------------------------------------------
Vec2_i32
X11_Window::GetMousePosition() const
{

}

//------------------------------------------------------------------------------
void
X11_Window::SetTitle(String const &str)
{
    XTextProperty prop;
    prop.value    = (unsigned char*)str.CStr();
    prop.encoding = XA_STRING;
    prop.format   = 8;
    prop.nitems   = str.Length();
    XSetWMName(_display, _window, &prop);
}

//------------------------------------------------------------------------------
void
X11_Window::SetSize(u32 const width, u32 const height)
{
    XResizeWindow(_display, _window, width, height);
}

//------------------------------------------------------------------------------
void
X11_Window::Show()
{

}

//------------------------------------------------------------------------------
void
X11_Window::Hide()
{

}

//------------------------------------------------------------------------------
bool
X11_Window::HandleEvents(WindowEvent *event, bool const dispatch_underlying_system_event /* = false */)
{
    event->type = WindowEvent::Type::Invalid;
    _curr_event_weak_ptr = event;

    ark_local_persist bool __hack_for_first_time = false;
    if(__hack_for_first_time) {
        __hack_for_first_time = false;
        OnWindowCreate();
        return true;
    }

    XEvent x_event = {};
    u32 mask   = ExposureMask | ButtonPressMask | KeyPressMask;
    i32 result = XCheckMaskEvent(_display, mask, &x_event);
    if(result != True) {
        return false;
    }

    if(x_event.type == Expose && x_event.xexpose.count == 0) {
        OnWindowPaint();
        return true;
    }

    return false;
}

//------------------------------------------------------------------------------
bool
X11_Window::HandleEventsBlocking(WindowEvent *event, bool const dispatch_underlying_system_event /* = false */)
{

}

//------------------------------------------------------------------------------
void
X11_Window::CreateScreenBuffer(u32 const width, u32 const height, u32 const bpp)
{
    // First time around.. So create the offscreen buffer.
    if(!_screen_buf) {
        _screen_buf = ark::AllocMemory<OffscreenBuffer>(sizeof(OffscreenBuffer));
        ark::ResetMemory(_screen_buf);
    }

    // Check if the total area is bigger, if not we can just reuse the
    // current buffer but with different width / area
    // @todo(stdmatt): Check how to free the memory if needded - May 2, 21
    u32 const new_size = ARK_AREA(width, height) * bpp;
    u32 const old_size = ARK_AREA(_screen_buf->width, _screen_buf->height) * _screen_buf->bytes_per_pixel;
    if(new_size > old_size) {
        ark::FreeMemory(_screen_buf->memory);
        _screen_buf->memory = ark::AllocMemory<u8>(new_size);
    }

    _screen_buf->width           = width;
    _screen_buf->height          = height;
    _screen_buf->pitch           = bpp * width;
    _screen_buf->bytes_per_pixel = bpp;
}

//------------------------------------------------------------------------------
OffscreenBuffer*
X11_Window::GetCurrentScreenBuffer()
{
    return _screen_buf;
}

//------------------------------------------------------------------------------
void
X11_Window::FlushBuffers()
{
    // Taken from:
    // https://chromium.googlesource.com/chromium/chromium/+/refs/heads/trunk/media/tools/player_x11/x11_video_renderer.cc
    // https://stackoverflow.com/questions/66885643/how-to-render-a-scaled-pixel-buffer-with-xlib-xrender
    Vec2_i32 const screen_size = GetClientSize();
    Vec2_i32 const buffer_size(_screen_buf->width, _screen_buf->height);

    // Creates a XImage.
    XImage image;
    memset(&image, 0, sizeof(image));
    image.width            = _screen_buf->width;
    image.height           = _screen_buf->height;
    image.data             = Cast<char *>(_screen_buf->memory);
    image.bytes_per_line   = _screen_buf->pitch;
    image.bits_per_pixel   = _screen_buf->bytes_per_pixel * 8;
    image.depth            = 32;
    image.format           = ZPixmap;
    image.byte_order       = LSBFirst;
    image.bitmap_unit      = 8;
    image.bitmap_bit_order = LSBFirst;
    image.red_mask         = 0xff;
    image.green_mask       = 0xff00;
    image.blue_mask        = 0xff0000;

    // Creates a pixmap and uploads from the XImage.
    Pixmap pixmap = XCreatePixmap(_display, _window, buffer_size.x, buffer_size.y, image.depth);
    GC     gc     = XCreateGC    (_display, pixmap, 0, nullptr);

    XPutImage(
        _display, pixmap, gc, &image,
        0, 0,
        0, 0,
        buffer_size.x, buffer_size.y
    );

    // Creates the picture representing the pixmap.
    Picture picture = XRenderCreatePicture(_display, pixmap, GetRenderARGB32Format(_display), 0, nullptr);

    // Composite the picture over the picture representing the window.
    f32 const x_scale = f32(buffer_size.x) / f32(screen_size.x);
    f32 const y_scale = f32(buffer_size.y) / f32(screen_size.y);
    XTransform transform_matrix = {{
        {XDoubleToFixed(x_scale), XDoubleToFixed(0.0f),    XDoubleToFixed(0.0f)},
        {XDoubleToFixed(0.0f),    XDoubleToFixed(y_scale), XDoubleToFixed(0.0f)},
        {XDoubleToFixed(0.0f),    XDoubleToFixed(0.0f),    XDoubleToFixed(1.0f)}
    }};
    XRenderSetPictureTransform(_display, picture, &transform_matrix);
    XRenderComposite(
        _display, PictOpSrc, picture, 0, _render_picture,
        0, 0, 0, 0, 0, 0,
        screen_size.x, screen_size.y
    );

    // Free the resources....
    XFreeGC           (_display, gc);
    XRenderFreePicture(_display, picture);
    XFreePixmap       (_display, pixmap);
}

//
// Helper Methods
//
//------------------------------------------------------------------------------
void
X11_Window::InitWithOptions(Window::CreateOptions const &options)
{
    _display = XOpenDisplay(nullptr);
    _screen  = DefaultScreen(_display);

    u32 const black_pixel = BlackPixel(_display, _screen);
    u32 const white_pixel = WhitePixel(_display, _screen);

    _window = XCreateSimpleWindow(
        _display,
        DefaultRootWindow(_display),         // Parent
        0, 0, options.width, options.height, // Window Rect
        0,                                   // Border width
        black_pixel,
        white_pixel
    );

    GC gc = XCreateGC(_display, _window, 0, nullptr);
    XSetStandardProperties(
        _display, _window,
        options.caption.CStr(), // Window Title
        nullptr,                // Window Icon
        x11_None,               // Icon Bitmap
        nullptr,                // argc
        0,                      // argv
        nullptr                 // hints
    );

    XSelectInput   (_display, _window, ExposureMask|ButtonPressMask|KeyPressMask);
    XSetBackground (_display, gc, white_pixel);
    XSetForeground (_display, gc, black_pixel);
    XClearWindow   (_display, _window);
    XMapRaised     (_display, _window);

    XWindowAttributes attr;
    XRenderPictFormat *pict_format = nullptr;

    XGetWindowAttributes(_display, _window, &attr);
    pict_format     = XRenderFindVisualFormat(_display, attr.visual);
    _render_picture = XRenderCreatePicture   (_display, _window, pict_format, 0, nullptr);

    XFreeGC(_display, gc);

    // Create the screen buffer.
    CreateScreenBuffer(options.buffer_width, options.buffer_height, options.buffer_bpp);
}

//
// X11 Event Handlers
//
//------------------------------------------------------------------------------
void
X11_Window::OnWindowCreate()
{
    if(!_curr_event_weak_ptr) {
        return;
    }
    _curr_event_weak_ptr->type = ark::Gfx::WindowEvent::Type::Create;
    ark::Gfx::WindowEvent::Create &data = _curr_event_weak_ptr->create_data;

    auto const &size = GetClientSize();
    data.width  = size.x;
    data.height = size.y;
}

//------------------------------------------------------------------------------
void
X11_Window::OnWindowPaint()
{
    FlushBuffers();
}

//------------------------------------------------------------------------------
void
X11_Window::OnWindowResize()
{
    // Setup the WindowEvent object if we have one...
    if(!_curr_event_weak_ptr) {
        return;
    }

    _curr_event_weak_ptr->type = ark::Gfx::WindowEvent::Type::Resize;
    ark::Gfx::WindowEvent::Resize &data = _curr_event_weak_ptr->resize_data;

    auto const &size = GetClientSize();
    data.new_width  = size.x;
    data.new_height = size.y;
}

} // namespace Gfx
} // namspace ark
#endif // ARK_OS_IS_GNU_LINUX
