#pragma once

#include "ark_core/Platform/Discovery.hpp"
#if ARK_OS_IS_GNU_LINUX

namespace ark { namespace Gfx { namespace GnuLinux {

/// @brief
///   Encapsulate the raw even from window of GNU/Linux platform.
///   This is used on the Window::HandleEvents to capture things
///   that we need, but isn't cross-platform enough to be put into ark.
struct WindowUnderlyingSystemEvent {
};

} // namespace GnuLinux

using WindowUnderlyingSystemEvent_t = GnuLinux::WindowUnderlyingSystemEvent;

} // namespace Gfx
} // namespace ark

#endif // ARK_OS_IS_GNU_LINUX
