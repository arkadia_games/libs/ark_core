#pragma once
#include "ark_core/Platform/Discovery.hpp"
#if (ARK_CURRENT_OS == ARK_OS_GNU_LINUX)
// XLibs
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xrender.h>
constexpr auto x11_Always = Always;
constexpr auto x11_None   = None;
#undef Always
#undef None
#endif // (ARK_CURRENT_OS == ARK_OS_GNU_LINUX)
