#pragma once

// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Math.hpp"
#include "ark_core/String.hpp"

namespace ark { namespace Gfx {

struct OffscreenBuffer;
struct WindowEvent;

class Window
{
    ARK_DISALLOW_COPY_AND_MOVE(Window);

    //
    // CTOR / DTOR
    //
protected:
    Window()          = default;
    virtual ~Window() = default;

    //
    // Inner Types
    //
public:
    struct CreateOptions final {
        u32         width;
        u32         height;
        ark::String caption;

        u32 buffer_width;
        u32 buffer_height;
        u32 buffer_bpp;
    };

    //
    // Factory
    //
public:
    static Window*       Create                 (CreateOptions const &create_options);
    static void          Destroy                (Window *&window);
    static CreateOptions GetDefaultCreateOptions();

    //
    // Public Methods
    //
public:
    virtual void            * GetNativeHandle     () const = 0;
    virtual u32               GetWidth            () const = 0;
    virtual u32               GetHeight           () const = 0;
    virtual Rect_i32          GetClientRect       () const = 0;
    virtual Vec2_i32          GetClientSize       () const = 0;
    virtual String            GetTitle            () const = 0;
    virtual bool              WantsToClose        () const = 0;
    virtual Vec2_i32          GetMousePosition    () const = 0;

    virtual void SetTitle(String const &str)                    = 0;
    virtual void SetSize (u32    const width, u32 const height) = 0;

    virtual void Show() = 0;
    virtual void Hide() = 0;

    virtual bool HandleEvents        (WindowEvent *event, bool const dispatch_underlying_system_event = false) = 0;
    virtual bool HandleEventsBlocking(WindowEvent *event, bool const dispatch_underlying_system_event = false) = 0;

    virtual void CreateScreenBuffer(
        u32 const width,
        u32 const height,
        u32 const bpp
    ) = 0;
    virtual OffscreenBuffer* GetCurrentScreenBuffer() = 0;
    virtual void             FlushBuffers          () = 0;
};

} // namespace Gfx
} // namespace ark
