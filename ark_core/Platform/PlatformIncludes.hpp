﻿#pragma once
#include "ark_core/Platform/Discovery.hpp"
#if (ARK_CURRENT_OS == ARK_OS_GNU_LINUX)
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pwd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <algorithm>
#include <time.h>

#elif (ARK_CURRENT_OS == ARK_OS_WINDOWS)

#include <corecrt_io.h>
#include <direct.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <algorithm>
// Windows
#define WINDOWS_LEAN_AND_MEAN
#include <windows.h>
#include <Shlobj.h>
#include <shlwapi.h>
#include <psapi.h>
// Libs
#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib, "User32.lib" )
#pragma comment(lib, "psapi.lib"  )
// Undefine windows stuff...
#undef CreateFile
#undef CreateDirectory
#undef DeleteFile
#undef LoadIcon
#undef CopyMemory
#undef GetClassName
#undef max
#undef min
#undef NO_ERROR
#undef SUCCESS

#elif (ARK_CURRENT_OS == ARK_OS_DOS)

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pwd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <algorithm>
#include <time.h>

#endif  //  #if (ARK_CURRENT_OS == ARK_OS_GNU_LINUX)
