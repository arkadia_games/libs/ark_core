#pragma once
// Arkadia
#include "ark_core/Error.hpp"
#include "ark_core/Containers/String.hpp"

namespace ark { namespace Platform {

//
// Error Management
//
//------------------------------------------------------------------------------
constexpr i32 INVALID_ERROR_CODE = ark::Max<i32>();

i32         GetPlatformLastError          ();
ark::String GetPlatformLastErrorAsString  (i32 error_code = INVALID_ERROR_CODE);
ark::Error  GetPlatformLastErrorAsArkError(i32 error_code = INVALID_ERROR_CODE);

ark::ErrorCodes::Codes TranslatePlatformErrorToArk(i32 const err);


i32 GetCLibLastError();
ark::String GetCLibLastErrorAsString  (i32 error_code = INVALID_ERROR_CODE);
ark::Error  GetCLibLastErrorAsArkError(i32 error_code = INVALID_ERROR_CODE);

ark::ErrorCodes::Codes TranslateCLibErrorToArk(i32 const err);



//
//
//
//------------------------------------------------------------------------------
//
// Defined at each platform.
// Get executable name.
ark::String GetProgramName(bool const remove_extension = true);

//
// Defined at each platform.
// Get the directory of the executable.
ark::String GetProgramDir();

} // Platform
} // ark


//
// Include the actual platform header.
//
#include "ark_core/Platform/Discovery.hpp"
#if defined(ARK_OS_IS_WINDOWS)
    #include "ark_core/Platform/Win32/Platform.Win32.hpp"
#elif defined(ARK_OS_IS_GNU_LINUX)
    #include "ark_core/Platform/GnuLinux/Platform.GnuLinux.hpp"
#elif defined(ARK_OS_IS_DOS)
    #include "ark_core/Platform/DOS/Platform.DOS.hpp"
#endif
