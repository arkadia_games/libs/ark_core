#pragma once
#include "ark_core/Platform/Discovery.hpp"
#if (ARK_CURRENT_OS == ARK_OS_WINDOWS)
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Debug.hpp"
#include "ark_core/Input.hpp"
#include "ark_core/Platform/PlatformIncludes.hpp"

namespace ark { namespace Win32 {

//
// Memory Management
//
//------------------------------------------------------------------------------
// @todo(stdmatt): Using VirtualAlloc for now, but in future maybe let uer
// decided the type of allocation - Jan 30, 21
void* AllocMemory  (size_t const size);
void  ReleaseMemory(void *&memory);

//------------------------------------------------------------------------------
template <typename Return_Type>
ARK_FORCE_INLINE Return_Type*
AllocMemory(size_t const size_in_bytes)
{
    void *memory = AllocMemory(size_in_bytes);
    return Cast<Return_Type*>(memory);
}

//------------------------------------------------------------------------------
template <typename T>
ARK_FORCE_INLINE void
ReleaseMemory(T *&memory)
{
    void *mem = Cast<void*>(memory);
    ReleaseMemory(mem);
    memory = nullptr;
}

//
// Process Management
//
//------------------------------------------------------------------------------
using ProcessId_t = DWORD;
ark::String FindExecutableNameFromProcessId(ProcessId_t const process_id);

//
// Time Functions
//
//------------------------------------------------------------------------------
u32      FileTime_to_POSIX(FILETIME    const file_time);
FILETIME POSIX_to_FileTime(ark::time_t const posix_time);

//
// File Functions
//
//------------------------------------------------------------------------------
HANDLE GetFileHandle(ark::String const &path);

//
// Misc Functions
//
//------------------------------------------------------------------------------
HICON LoadIconFromFile(HINSTANCE const instance, ark::String const &filename);

//------------------------------------------------------------------------------
ark::String GetClassName(HWND const handle);

//
// Input Functions
//
//------------------------------------------------------------------------------
bool               IsKeyboardWM                    (u32 msg_type);
ark::KeyboardEvent TranslateWin32KeyToKeyboardEvent(u32 const msg_type, u32 const virtual_key_code, u32 const flags);

//
// Window Functions
//
//------------------------------------------------------------------------------
void SetWindowWidth (HWND const window_handle, u32 const value);
void SetWindowHeight(HWND const window_handle, u32 const value);


} // namespace Win32
} // namespace ark

#endif // (ARK_CURRENT_OS == ARK_OS_WINDOWS)
