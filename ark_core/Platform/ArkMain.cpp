#ifndef NO_ARK_MAIN
#include "ark_core/Platform.hpp"
#include "ark_core/Terminal.hpp"

#if ARK_CURRENT_OS == ARK_OS_WINDOWS

//------------------------------------------------------------------------------
int WINAPI
WinMain(
    HINSTANCE instance,
    HINSTANCE prev_instance,
    PSTR      command_line,
    int       command_show)
{
    ARK_UNUSED(prev_instance);

    ARK_EXCEPTION_CATCH_ALL_ALL_IN_MAIN({
        ark::String command_line_with_prog_name = ark::String::Format(
            "{} {}",
            ark::Platform::GetProgramName(),
            command_line
        );

        command_line_with_prog_name.Trim();
        ark::CMD::Set(command_line_with_prog_name);
        ark_main(command_line_with_prog_name);
    }); // ARK_EXCEPTION_CALL_ALL_IN_MAIN
}


#endif // ARK_CURRENT_OS == ARK_OS_WINDOWS

//------------------------------------------------------------------------------
int
main(int const argc, char const *argv[])
{
    ark::CMD::Set(argc, argv);
    ark_main(ark::CMD::Get());
}

#endif // #ifndef NO_ARK_MAIN
