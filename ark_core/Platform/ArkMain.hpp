#pragma once

#ifndef NO_ARK_MAIN

// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Containers/String.hpp"

//
// Entry point of every program ;D
//
i32 ark_main(ark::String const &command_line);

#endif
