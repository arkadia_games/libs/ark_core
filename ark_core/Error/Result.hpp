#pragma once

// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Error/Error.hpp"

namespace ark {

template <typename Value_Type, typename Error_Type = Error>
class Result
{
public:
    Value_Type value;

public:
    ARK_FORCE_INLINE bool               HasSucceeded() const { return  _success; }
    ARK_FORCE_INLINE bool               HasFailed   () const { return !_success; }
    ARK_FORCE_INLINE Error_Type const & GetError    () const { return  _error;   }

    ARK_FORCE_INLINE static Result<Value_Type, Error_Type>
    Success(Value_Type const &value)
    {
        return Result<Value_Type, Error_Type>(value);
    }

    ARK_FORCE_INLINE static Result<Value_Type, Error_Type>
    Fail(Error_Type const &error)
    {
        return Result<Value_Type, Error_Type>(error);
    }

private:
    Result(Value_Type const &value) :  value(value), _success(true ) {}
    Result(Error_Type const &error) : _error(error), _success(false) {}

private:
    bool       const _success;
    Error_Type const _error;
}; // class Result

} // namespace ark
