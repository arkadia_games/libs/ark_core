#pragma once

// std
#include <vector>
// Arkadia
#include "ark_core/Algo.hpp"

namespace ark {

template <typename Item_Type>
class Array
{
    //
    // Enums / Constants / Typedefs
    //
private:
    typedef typename std::vector<Item_Type> _container_type_;
    _container_type_ _container;

public:
    using value_type      = typename _container_type_::value_type;
    using size_type       = typename _container_type_::size_type;
    using difference_type = typename _container_type_::difference_type;
    using pointer         = typename _container_type_::pointer;
    using const_pointer   = typename _container_type_::const_pointer;
    using reference       = value_type&;
    using const_reference = const value_type&;

    using iterator       = typename _container_type_::iterator;
    using const_iterator = typename _container_type_::const_iterator;

    using reverse_iterator       = typename _container_type_::reverse_iterator;
    using const_reverse_iterator = typename _container_type_::const_reverse_iterator;

public:
    static const size_t InvalidIndex = NumericLimits<size_t>::Max;

    enum class RemoveOptions {
        PreserveOrder,
        IgnoreOrder
    }; // enum RemoveOptions


    //
    // Static Methods
    //
public:
    //--------------------------------------------------------------------------
    static Array<Item_Type>
    CreateWithCapacity(size_t const capacity)
    {
        Array<Item_Type> s;
        s.Reserve(capacity);
        return s;
    }

    //
    // CTOR / DTOR
    //
public:
    //--------------------------------------------------------------------------
    Array() = default;

    //--------------------------------------------------------------------------
    Array(std::initializer_list<Item_Type> const &init_list)
        : _container(init_list)
    {
        // Empty...
    }


    //
    // Public Methods
    //
public:
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE Item_Type       & At(size_t const i)       { ARK_ASSERT(i < Count(), "Array out of bounds: ({0}) of {1})", i, Count()); return _container[i]; }
    ARK_FORCE_INLINE Item_Type const & At(size_t const i) const { ARK_ASSERT(i < Count(), "Array out of bounds: ({0}) of {1})", i, Count()); return _container[i]; }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE Item_Type       & Back()       { return _container.back(); }
    ARK_FORCE_INLINE Item_Type const & Back() const { return _container.back(); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Clear() { _container.clear(); }
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE size_t Count() const { return _container.size (); }

    //--------------------------------------------------------------------------
    template<class... _Valty> void
    ARK_FORCE_INLINE
    EmplaceBack(_Valty&&... _Val)
    {
        _container.emplace_back(_Val...);
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE Item_Type       & Front()       { return _container.front(); }
    ARK_FORCE_INLINE Item_Type const & Front() const { return _container.front(); }

    //--------------------------------------------------------------------------
    Item_Type* GetBuffer() { return _container.data(); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE bool IsEmpty() const { return _container.empty(); }

    //--------------------------------------------------------------------------
    size_t
    IndexOf(Item_Type const &item) const
    {
        for(size_t i = 0, count = Count(); i < count; ++i) {
            if(_container.operator[](i) == item) {
                return i;
            }
        }
        return Array<Item_Type>::InvalidIndex;
    }

    //--------------------------------------------------------------------------
    template <typename Func_Type>
    size_t
    IndexOf(Func_Type const &func) const
    {
        for(size_t i = 0, count = Count(); i < count; ++i) {
            if(func(_container.operator[](i))) {
                return i;
            }
        }
        return Array<Item_Type>::InvalidIndex;
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void PopBack() { _container.pop_back ();  }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void PushBack(Array<Item_Type> const &t)
    { Algo::Append(*this, t); }

    ARK_FORCE_INLINE void PushBack(std::initializer_list<Item_Type> const &t)
    { Algo::Append(*this, t); }

    ARK_FORCE_INLINE void PushBack(Item_Type const &t = Item_Type())
    { _container.push_back(t); }

    ARK_FORCE_INLINE void PushBack(Item_Type &&t)
    { _container.push_back(std::move(t)); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Remove(Item_Type const &value)
    {
        _container.erase(std::remove(_container.begin(), _container.end(), value));
    }

    //--------------------------------------------------------------------------
    void
    RemoveAt(
        size_t        const index,
        RemoveOptions const &remove_options = RemoveOptions::PreserveOrder)
    {
        if(remove_options == RemoveOptions::IgnoreOrder) {
            std::swap(_container.operator[](index), _container.back());
            _container.pop_back();
        }
        else {
            _container.erase(begin() + index);
        }
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Resize (size_t const size) { _container.resize (size); }
    ARK_FORCE_INLINE void Reserve(size_t const size) { _container.reserve(size); }

    //
    // Operators
    //
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE Array<Item_Type>& operator+=(Item_Type const &rhs) { PushBack(rhs); return *this; }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE typename _container_type_::const_reference operator[](size_t const index) const { return _container[index]; }
    ARK_FORCE_INLINE typename _container_type_::reference       operator[](size_t const index)       { return _container[index]; }

    //
    // Iterator Methods
    //
public:
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE iterator       begin() noexcept       { return _container.begin(); }
    ARK_FORCE_INLINE const_iterator begin() const noexcept { return _container.begin(); }
    ARK_FORCE_INLINE iterator       end  () noexcept       { return _container.end  (); }
    ARK_FORCE_INLINE const_iterator end  () const noexcept { return _container.end  (); }

    ARK_FORCE_INLINE reverse_iterator       rbegin() noexcept       { return _container.rbegin(); }
    ARK_FORCE_INLINE const_reverse_iterator rbegin() const noexcept { return _container.rbegin(); }
    ARK_FORCE_INLINE reverse_iterator       rend  () noexcept       { return _container.rend  (); }
    ARK_FORCE_INLINE const_reverse_iterator rend  () const noexcept { return _container.rend  (); }

}; // class Array
}  // namespace ark
