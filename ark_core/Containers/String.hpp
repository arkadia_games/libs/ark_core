//----------------------------------------------------------------------------//
//                                          █████                             //
//                                         ░░███                              //
//                       ██████   ████████  ░███ █████                        //
//                      ░░░░░███ ░░███░░███ ░███░░███                         //
//                       ███████  ░███ ░░░  ░██████░                          //
//                      ███░░███  ░███      ░███░░███                         //
//                     ░░████████ █████     ████ █████                        //
//                      ░░░░░░░░ ░░░░░     ░░░░ ░░░░░                         //
//                                                                            //
//  File    : String.hpp                                                      //
//  Project : ark_core                                                        //
//  Date    : Jan 25, 2021                                                    //
//  License : GPLv3 (Check COPYING.txt for details)                           //
//  Author  : stdmatt - matt@arkadia.games                                    //
//                                                                            //
//  Description:                                                              //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once

// std
#include <string.h>
#include <ctype.h>
#include <string>
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Containers/Array.hpp"

namespace ark {

//
// Constants
//
//------------------------------------------------------------------------------
ARK_STRICT_CONSTEXPR size_t INVALID_STRING_INDEX = std::string::npos;

//
// C - String
//
//------------------------------------------------------------------------------
ARK_FORCE_INLINE size_t
CStrLen(const char * const str)
{
    if(!str) {
        return 0;
    }
    return strlen(str);
}

//------------------------------------------------------------------------------
ARK_FORCE_INLINE bool
CStrEquals(const char * const lhs, char const * const rhs)
{
    return strcmp(lhs, rhs) == 0;
}

//------------------------------------------------------------------------------
ARK_FORCE_INLINE bool
CStrIsNullEmptyOrWhitespace(char const * const str)
{
    if(!str) {
        return true;
    }
    if(CStrLen(str) == 0) {
        return true;
    }
    for(char const *c = str; c != nullptr; ++c) {
        if(*c != ' ') {
            return false;
        }
    }
    return true;
}

//------------------------------------------------------------------------------
ARK_FORCE_INLINE void
CStrMemCopy(
    char         const *dst,
    size_t       const offset,
    char const * const src,
    size_t       const src_size)
{
    ::memcpy(Cast<void*>(dst + offset), src, src_size);
}



//
// String
//
// @todo(stdmatt): Enventually move away from std::string ...
class String
{
private:
    typedef std::string _container_type_;
    _container_type_ _container;
public:
    using value_type      = _container_type_::value_type;
    using size_type       = _container_type_::size_type;
    using difference_type = _container_type_::difference_type;
    using pointer         = _container_type_::pointer;
    using const_pointer   = _container_type_::const_pointer;
    using reference       = value_type&;
    using const_reference = const value_type&;

    using iterator       = _container_type_::iterator;
    using const_iterator = _container_type_::const_iterator;

    using reverse_iterator       = _container_type_::reverse_iterator;
    using const_reverse_iterator = _container_type_::const_reverse_iterator;

    //
    // Static Methods
    //
public:
    //--------------------------------------------------------------------------
    template <typename ...Args>
    static String Concat(Args ...args)
    {
        String s;
        s.Concat({args...});
        return s;
    }

    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    ARK_INLINE static bool
    ConvertToU32(String const &str, u32 *out_value)
    {
        // @todo(stdmatt): Make it right...
        ARK_SAFE_ASSIGN(out_value, atoi(str.CStr()));
        return true;
    }

    //--------------------------------------------------------------------------
    ARK_INLINE static String
    CreateWithLength(size_t const len)
    {
        String s;
        s.Resize(len);
        return s;
    }

    //--------------------------------------------------------------------------
    ARK_INLINE static String
    CreateWithCapacity(size_t const capacity)
    {
        String s;
        s.Reserve(capacity);
        return s;
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE static String Empty() { return {}; }

    //--------------------------------------------------------------------------
    template <typename ...Args>
    static String
    Format(String const &fmt, Args... args)
    {
        return fmt::format(fmt.CStr(), args...);
    }

    //--------------------------------------------------------------------------
    static String
    Repeat(String const &what, size_t const times)
    {
        if(times == 0) {
            return String::Empty();
        }

        size_t const repeat_len = what.Length();
        size_t const result_len = times * repeat_len;
        String       result     = String::CreateWithLength(result_len);

        for(size_t i = 0; i < result_len; i += repeat_len) {
            result.MemCopy(i, what.CStr(), repeat_len);
        }

        return result;
    }

    //--------------------------------------------------------------------------
    template <typename TypeIterator, typename = TypeTraits::RequireInputIterator<TypeIterator>>
    static String
    Join(
        TypeIterator const &beg,
        TypeIterator const &end,
        char         const separator = ' ')
    {
        TypeIterator curr = beg;

        size_t size_to_reserve = 0;
        while(curr != end) {
            size_to_reserve += (*curr).Length() + 1; // Separator Size.
            ++curr;
        }

        String s = String::CreateWithLength(size_to_reserve);
        size_t offset = 0;

        curr = beg;
        while(curr != end) {
            String const &component     = *curr;
            size_t const  component_len = component.Length();

            s.MemCopy(offset, component.CStr(), component_len);
            offset += component_len;

            s[offset] = separator;
            offset += 1;

            ++curr;
        }

        return s;
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE static String
    Join(Array<String> const &components, char const separator = ' ')
    {
        return Join(components.begin(), components.end(), separator);
    }

    //
    // Operators
    //
public:
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE bool operator==(String const      &rhs) const { return this->StdString() == rhs.StdString(); }
    ARK_FORCE_INLINE bool operator==(char const * const str) const { return CStrEquals(this->CStr(), str);        }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE bool operator!=(String const      &rhs) const { return this->StdString() != rhs.StdString(); }
    ARK_FORCE_INLINE bool operator!=(char const * const str) const { return !CStrEquals(this->CStr(), str);       }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE bool operator<(String const      &rhs) const { return this->StdString() < rhs.StdString(); }
    ARK_FORCE_INLINE bool operator<(char const * const str) const { return strcmp(this->CStr(), str) < 0;       }


    ARK_FORCE_INLINE String& operator+=(String const      &rhs) { Append(rhs); return *this; }
    ARK_FORCE_INLINE String& operator+=(char const * const str) { Append(str); return *this; }
    ARK_FORCE_INLINE String& operator+=(char         const c  ) { Append(c  ); return *this; }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE String operator+(const String      &rhs) const { String s(*this); s.Append(rhs); return s; }
    ARK_FORCE_INLINE String operator+(char const * const str) const { String s(*this); s.Append(str); return s; }
    ARK_FORCE_INLINE String operator+(char         const c  ) const { String s(*this); s.Append(c  ); return s; }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE _container_type_::const_reference operator[](size_t const index) const { return _container[index]; }
    ARK_FORCE_INLINE _container_type_::reference       operator[](size_t const index)       { return _container[index]; }

    //
    // CTOR / DTOR
    //
public:
    //--------------------------------------------------------------------------
    ARK_INLINE explicit String(i8  const i) : _container(std::to_string(i)) {}
    ARK_INLINE explicit String(u8  const i) : _container(std::to_string(i)) {}
    ARK_INLINE explicit String(i16 const i) : _container(std::to_string(i)) {}
    ARK_INLINE explicit String(u16 const i) : _container(std::to_string(i)) {}
    ARK_INLINE explicit String(i32 const i) : _container(std::to_string(i)) {}
    ARK_INLINE explicit String(u32 const i) : _container(std::to_string(i)) {}

    //--------------------------------------------------------------------------
    ARK_INLINE
    String(std::string const &str)
        : _container(str)
    {
        // Empty...
    }

    //--------------------------------------------------------------------------
    ARK_INLINE
    String(char const * const str = "")
        : _container(str)
    {
        // Empty...
    }

    //--------------------------------------------------------------------------
    ARK_INLINE explicit
    String(char const c)
    {
        _container.push_back(c);
    }


    //
    //
    //
    //--------------------------------------------------------------------------
public:
    ARK_FORCE_INLINE char        const * CStr     () const { return _container.c_str(); }
    ARK_FORCE_INLINE std::string const & StdString() const { return _container;         }

    //
    //
    //
public:
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Append(String       const str) { _container.append(str.CStr()); }
    ARK_FORCE_INLINE void Append(char const * const str) { _container.append(str);        }
    ARK_FORCE_INLINE void Append(char const         c  ) { _container.push_back(c);       }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Clear() { _container.clear(); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Concat(String const &str) { this->Append(str); }

    //--------------------------------------------------------------------------
    ARK_INLINE void
    Concat(std::initializer_list<String> const &init_list)
    {
        for(auto const &s : init_list) {
            this->Append(s);
        }
    }

    //--------------------------------------------------------------------------
    bool EndsWith(String const &needle) const;
    bool EndsWith(char   const  needle) const;

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE bool IsEmpty() const { return _container.empty(); }

    //--------------------------------------------------------------------------
    ARK_INLINE bool
    IsEmptyOrWhitespace() const
    {
        if(IsEmpty()) {
            return true;
        }

        size_t const index = FindFirstIndexNotOf(' ');
        return index == INVALID_STRING_INDEX;
    }

    //--------------------------------------------------------------------------
    size_t FindIndexOf(char        const  needle, size_t const start_index = 0) const;
    size_t FindIndexOf(ark::String const &needle, size_t const start_index = 0) const;

    //--------------------------------------------------------------------------
    size_t FindFirstIndexNotOf(char        const  needle, size_t const start_index = 0) const;
    size_t FindFirstIndexNotOf(ark::String const &needle, size_t const start_index = 0) const;

    //--------------------------------------------------------------------------
    size_t FindLastIndexOf(char        const  needle, size_t const start_index = 0) const;
    size_t FindLastIndexOf(ark::String const &needle, size_t const start_index = 0) const;

    //--------------------------------------------------------------------------
    size_t FindLastIndexNotOf(char        const  needle, size_t const start_index = 0) const;
    size_t FindLastIndexNotOf(ark::String const &needle, size_t const start_index = 0) const;

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE size_t Length() const { return _container.size (); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void
    MemCopy(
        size_t       const offset,
        char const * const mem_to_copy,
        size_t       const size_to_copy)
    {
        CStrMemCopy(CStr(), offset, mem_to_copy, size_to_copy);
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void PushBack(String const &rhs) { Append(rhs); }
    ARK_FORCE_INLINE void PushBack(char   const  rhs) { _container.push_back(rhs); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void PopBack() { _container.pop_back(); }

    //--------------------------------------------------------------------------
    bool StartsWith(String const &needle) const;
    bool StartsWith(char   const  needle) const;

    //--------------------------------------------------------------------------
    Array<String> Split(char        const  separator ) const;
    Array<String> Split(Array<char> const &separators) const;

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE String
    SubString(size_t const left_index, size_t const right_index) const
    {
        return _container.substr(left_index, right_index - left_index);
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Reserve(size_t const capacity) { _container.reserve(capacity); }
    ARK_FORCE_INLINE void Resize (size_t const size)     { _container.resize (size);     }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void Remove(ark::String const &what) { Replace(what, String::Empty()); }

    //--------------------------------------------------------------------------
    void Replace(ark::String const &what, ark::String const &with);

    //--------------------------------------------------------------------------
    ARK_INLINE void
    TrimLeft(char const char_to_trim = ' ')
    {
        size_t const left_index = FindFirstIndexNotOf(char_to_trim);
        if (left_index == INVALID_STRING_INDEX) {
            return;
        }
        // @todo(stdmatt): We are creating too much copies... Dec 20, 2020
        _container.operator=(SubString(left_index, Length()).CStr());
    }

    //--------------------------------------------------------------------------
    ARK_INLINE void
    TrimRight(char const char_to_trim = ' ')
    {
        size_t const right_index = FindLastIndexNotOf(char_to_trim);
        if (right_index == INVALID_STRING_INDEX) {
            return;
        }
        // @todo(stdmatt): We are creating too much copies... Dec 20, 2020a
        _container.operator=((SubString(0, right_index + 1)).CStr());
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE void
    Trim(char const char_to_trim = ' ')
    {
        TrimLeft (char_to_trim);
        TrimRight(char_to_trim);
    }

    //--------------------------------------------------------------------------
    ARK_INLINE void
    ToLower()
    {
        size_t const len = Length();
        for (size_t i = 0; i < len; ++i) {
            _container[i] = tolower(_container[i]);
        }
    }

    //--------------------------------------------------------------------------
    ARK_INLINE void
    ToUpper()
    {
        size_t const len = Length();
        for (size_t i = 0; i < len; ++i) {
            _container[i] = toupper(_container[i]);
        }
    }

    //
    // Iterator Methods
    //
public:
    _container_type_::reference       Back()       { return _container.back(); }
    _container_type_::const_reference Back() const { return _container.back(); }

    _container_type_::reference       Front()       { return _container.front(); }
    _container_type_::const_reference Front() const { return _container.front(); }

    _container_type_::iterator       begin() noexcept       { return _container.begin(); }
    _container_type_::const_iterator begin() const noexcept { return _container.begin(); }
    _container_type_::iterator       end  () noexcept       { return _container.end  (); }
    _container_type_::const_iterator end  () const noexcept { return _container.end  (); }

}; // class String


//
//
//
template <typename T>
ARK_FORCE_INLINE String ToString(T      const &       t) { return std::to_string(t); }
ARK_FORCE_INLINE String ToString(String const &       t) { return t;                 }
ARK_FORCE_INLINE String ToString(char   const * const t) { return String(t);         }
ARK_FORCE_INLINE String ToString(char         *       t) { return String(t);         }

template <typename FMT, typename ...Args>
ark::String Format(FMT fmt, Args ...args)
{
    return ark::String::Format(fmt, args...);
}

} // namespace ark

//
//
//
ARK_FMT_DEFINE_SIMPLE_FORMATTER(ark::String, "{}", var.CStr())
