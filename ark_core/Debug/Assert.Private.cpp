﻿// Header
#include "ark_core/Debug/Assert.Private.hpp"
// Arkadia
#include "ark_core/Debug/Debug.hpp"

//------------------------------------------------------------------------------
void
ark::Assert::Private::PrintMsgAndAbort(
    char const * const assert_name,
    char const * const file_name,
    int          const file_line,
    char const * const function_name,
    char const * const expression_str,
    char const * const user_msg)
{
    char const * const final_msg_format =
"Terminated with ({})\n"
"    File:     ({})\n"
"    Line:     ({})\n"
"    Function: ({})\n"
"    {}:{}\n"
"Expression:\n"
"    ({})\n"
"Message:\n"
"    {}\n";
    auto const formatted_str = fmt::format(
        final_msg_format,
        assert_name,
        file_name,
        file_line,
        function_name,
        file_name, file_line,
        expression_str,
        user_msg
    );

    printf("%s\n", formatted_str.c_str());
    fflush(stdout);
    ARK_DEBUG_BREAK();
    abort();
}
