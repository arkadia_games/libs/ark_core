#pragma once

// Arkadia - Private
#include "ark_core/Debug/Assert.Private.hpp"

#define ARK_STRINGIZE2(s) #s
#define ARK_STRINGIZE(s) ARK_STRINGIZE2(s)
#define ARK_CONCAT(a, b) ARK_STRINGIZE(a)ARK_STRINGIZE(b)

//
// Assert
//
#if (ARK_ASSERTION_IS_ENABLED)
    //--------------------------------------------------------------------------
    #define ARK_ASSERT_HELPER(_name_, _expr_, _fmt_, ...)            \
        do {                                                         \
            if(!(_expr_)) {                                          \
                ark::Assert::Private::PrintAssertionMessageAndAbort( \
                    _name_,                                          \
                    ARK_STRINGIZE((_expr_)),                         \
                    __FILE__,                                        \
                    __LINE__,                                        \
                    __FUNCTION__,                                    \
                    _fmt_,                                           \
                    ##__VA_ARGS__                                      \
                );                                                   \
            }                                                        \
        } while(0);

    //--------------------------------------------------------------------------
    #define ARK_ASSERT(_expr_, _fmt_, ...) \
        ARK_ASSERT_HELPER("ARK_ASSERT", _expr_, _fmt_, ##__VA_ARGS__);

    //--------------------------------------------------------------------------
    #define ARK_ASSERT_TRUE(_expr_) \
        ARK_ASSERT_HELPER("ARK_ASSERT_TRUE", (_expr_) != 0, "{} can't be false", ARK_STRINGIZE(_expr_));

    //--------------------------------------------------------------------------
    #define ARK_ASSERT_FALSE(_expr_) \
        ARK_ASSERT_HELPER("ARK_ASSERT_FALSE", (_expr_) == 0, "{} can't be false", ARK_STRINGIZE(_expr_));

    //--------------------------------------------------------------------------
    #define ARK_ASSERT_NULL(_expr_) \
        ARK_ASSERT_HELPER("ARK_ASSERT_NULL", (_expr_) == 0, "{} can't be null", ARK_STRINGIZE(_expr_));

//--------------------------------------------------------------------------
    #define ARK_ASSERT_NOT_NULL(_expr_) \
        ARK_ASSERT_HELPER("ARK_ASSERT_NOT_NULL", (_expr_) != 0, "{} can't be null", ARK_STRINGIZE(_expr_));

    //--------------------------------------------------------------------------
    #define ARK_ASSERT_NOT_ZERO(_expr_) \
        ARK_ASSERT_HELPER("ARK_ASSERT_NOT_ZERO", (_expr_) != 0, "{} can't be 0", ARK_STRINGIZE(_expr_));

#else // (ARK_ASSERTION_IS_ENABLED)
    #define ARK_ASSERT(_expr_, _fmt_, ...)          do { } while(0)
    #define ARK_ASSERT_TRUE(_expr_, _fmt_, ...)     do { } while(0)
    #define ARK_ASSERT_FALSE(_expr_, _fmt_, ...)    do { } while(0)
    #define ARK_ASSERT_NULL(_expr_, _fmt_, ...)     do { } while(0)
    #define ARK_ASSERT_NOT_NULL(_expr_, _fmt_, ...) do { } while(0)
    #define ARK_ASSERT_NOT_ZERO(_expr_, _fmt_, ...) do { } while(0)
#endif // ARK_ENABLE_ASSERTIONS)


//
// Verify
//
#define ARK_VERIFY(_expr_, _fmt_, ...)                               \
        do {                                                         \
            if(!(_expr_)) {                                          \
                ark::Assert::Private::PrintAssertionMessageAndAbort( \
                    "ARK_VERIFY",                                    \
                    ARK_STRINGIZE(_expr_),                           \
                    __FILE__,                                        \
                    __LINE__,                                        \
                    __FUNCTION__,                                    \
                    _fmt_                                            \
                    __VA_ARGS__                                      \
                );                                                   \
            }                                                        \
        } while(0);
