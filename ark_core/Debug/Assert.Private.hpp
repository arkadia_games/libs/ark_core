﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils/Inline.hpp"
#include "ark_core/CodeUtils/ArkFormat.hpp"

// Arkadia - Libs
namespace ark { namespace Assert { namespace Private {

//------------------------------------------------------------------------------
void
PrintMsgAndAbort(
    char const * const assert_name,
    char const * const file_name,
    int          const file_line,
    char const * const function_name,
    char const * const expression_str,
    char const * const user_msg);

//------------------------------------------------------------------------------
template <typename ...Args>
ARK_INLINE void
PrintAssertionMessageAndAbort(
    char const * const assert_name,
    char const * const expression_str,
    char const * const file_name,
    int          const file_line,
    char const * const function_name,
    char const * const fmt,
    Args               ...args)
{
    auto user_msg = fmt::format(fmt, std::forward<Args>(args)...);
    PrintMsgAndAbort(assert_name, file_name, file_line, function_name, expression_str, user_msg.c_str());
}


} // namespace Private
} // namespace Assert
} // namespace ark
