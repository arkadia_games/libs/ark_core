﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Math/Vec.hpp"
#include "ark_core/Containers/String.hpp"

namespace ark {

//
// Keyboard
//
//------------------------------------------------------------------------------
enum class KeyCodes {
    Up,
    Down,
    Left,
    Right,

    Esc,
    Return,

    BackTick,
}; // enum KeyCodes

struct KeyboardEvent {
    union {
        ark::KeyCodes keycode;
        u32           keycode_value;
    };

    u32  repeat_count;
    bool is_down;
    bool is_alt_down;
    bool is_control_down;
    bool is_shift_down;
}; // KeyboardEvent

inline ark::String
ToString(KeyboardEvent const &event)
{
    return ark::String::Format(
        "KeyboardEvent"
        "   code            : {}\n"
        "   repeat_count    : {}\n"
        "   is_down         : {}\n"
        "   is_alt_down     : {}\n"
        "   is_control_down : {}\n"
        "   is_shift_down   : {}\n",
        event.keycode_value,
        event.repeat_count,
        event.is_down,
        event.is_alt_down,
        event.is_control_down,
        event.is_shift_down
    );
}

//
// Mouse
//
//------------------------------------------------------------------------------
Vec2_i32 GetScreenMousePosition();

} // namespace ark
