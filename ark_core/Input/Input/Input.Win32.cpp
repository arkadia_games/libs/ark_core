﻿#include "ark_core/Platform/Discovery.hpp"
#if (ARK_CURRENT_OS == ARK_OS_WINDOWS)
// Header
#include "ark_Core/Input/InputCommon.hpp"
// Arkadia
#include "ark_core/Platform.hpp"

//------------------------------------------------------------------------------
ark::Vec2_i32
ark::GetScreenMousePosition()
{
    POINT p;
    if(!GetCursorPos(&p)) {
        // @todo(stdamtt): Handle errors - Mar 09, 21
    }
    return ark::Vec2_i32(p.x, p.y);
}

#endif // #if (ARK_CURRENT_OS == ARK_OS_WINDOWS)
