#pragma once

// std
#include <algorithm>
// Arkadia
#include "ark_core/CodeUtils.hpp"

namespace ark { namespace Algo {

template <typename T>
using Unqualified_Type = std::decay_t<std::remove_reference_t<T>>;


//------------------------------------------------------------------------------
template <typename Dst_Type, typename Src_Type>
ARK_FORCE_INLINE void
Append(Dst_Type &dst, Src_Type const &src)
{
    dst.Reserve(dst.Count() + src.Count());
    auto _end= src.end();
    for(auto it = src.begin(); it != _end; ++it) {
        dst.PushBack(*it);
    }
}

} // namespace Algo
} // namespace ark
