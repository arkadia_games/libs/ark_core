#pragma once

// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Platform.hpp"

namespace ark { namespace FileUtils {

//
// Read File
//
//------------------------------------------------------------------------------
typedef Result<Array<String>> ReadAllLinesResult_t;
ReadAllLinesResult_t ReadAllLines(String const &path);


//
// Write File
//
//------------------------------------------------------------------------------
enum class WriteMode {
    Append,
    Overwrite,
};

//------------------------------------------------------------------------------
typedef Result<size_t> WriteResult_t;
WriteResult_t WriteAllLines(
    String        const &path,
    Array<String> const &lines,
    String        const &new_line,
    WriteMode     const  write_mode);

//------------------------------------------------------------------------------
typedef Result<size_t> WriteResult_t;
WriteResult_t WriteBuffer(
    String     const &path,
    u8 const * const buffer,
    size_t     const buffer_size,
    WriteMode  const write_mode);

} // namespace FileUtils


//
// RAII FILE
//
class File final
{
public:
    ARK_INLINE File(ark::String const &filename, ark::String const &open_mode)
        : _filename(filename)
        , _open_mode(open_mode)
    {
        // Empty...
    }

    ARK_INLINE ~File()
    {
        if(_internal_handle) {
            fclose(_internal_handle);
        }
        _internal_handle = nullptr;
    }

public:
    ark::Result<bool> Open()
    {
        _internal_handle = ark::fopen(_filename.CStr(), _open_mode.CStr());
        if(!IsValid()) {
            return ark::Result<bool>::Fail(ark::Platform::GetCLibLastErrorAsArkError());
        }
        return ark::Result<bool>::Success(true);
    }

    ARK_FORCE_INLINE FILE* GetInternalHandle() const { return _internal_handle; }
    ARK_FORCE_INLINE bool  IsValid() const { return _internal_handle != nullptr; }


    ARK_INLINE void
    Close()
    {
        if(IsValid()) {
            fclose(_internal_handle);
        }
        _internal_handle = nullptr;
    }

private:
    FILE        * _internal_handle = nullptr;
    ark::String   _filename;
    ark::String   _open_mode;

}; // class File


} // namespace ark
