// Header
#include "FileUtils.hpp"
// std
#include <fstream>
// Arkadia
#include "ark_core/Path.hpp"
// Usings
namespace ark {

//
// Read File
//
//------------------------------------------------------------------------------
FileUtils::ReadAllLinesResult_t
FileUtils::ReadAllLines(String const &path)
{
    if(!PathUtils::IsFile(path)) {
        return ReadAllLinesResult_t::Fail({ ErrorCodes::FILEPATH_NOT_FOUND, path });
    }

    // @todo(stdmatt): Better error handling - 20 Dec, 2020...
    std::ifstream file(path.CStr());

    Array<String> lines; // @todo(stdmatt): Try to preallocate some memory... 20, Dec 2020.
    while(!file.eof()) {
        std::string s;
        std::getline(file, s);
        lines.PushBack(s);
    }

    return ReadAllLinesResult_t::Success(lines);
}

//
// Write File
//
//------------------------------------------------------------------------------
FileUtils::WriteResult_t
FileUtils::WriteAllLines(
    String        const &path,
    Array<String> const &lines,
    String        const &new_line,
    WriteMode     const  write_mode)
{
    ark::File file(
        path,
        (write_mode == WriteMode::Append) ? "ab" : "wb"
    );

    ark::Result<bool> const result = file.Open();
    if(result.HasFailed()) {
        return FileUtils::WriteResult_t::Fail(result.GetError());
    }

    size_t write_len = 0;
    for(String const &line : lines) {
        write_len += fprintf(file.GetInternalHandle(), "%s\n", line.CStr());
    }

    return WriteResult_t::Success(write_len);
}

//------------------------------------------------------------------------------
FileUtils::WriteResult_t
FileUtils::WriteBuffer(
    String     const &path,
    u8 const * const buffer,
    size_t     const buffer_size,
    WriteMode  const  write_mode)
{
    ark::File file(
        path,
        (write_mode == WriteMode::Append) ? "ab" : "wb"
    );

    ark::Result<bool> const result = file.Open();
    if(result.HasFailed()) {
        return FileUtils::WriteResult_t::Fail(result.GetError());
    }

    size_t const write_len = fwrite(buffer, sizeof(u8), buffer_size, file.GetInternalHandle());
    return WriteResult_t::Success(write_len);
}

} // namespace ark
