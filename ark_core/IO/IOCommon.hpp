﻿#pragma once

// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Containers/String.hpp"

namespace ark {

//------------------------------------------------------------------------------
template <typename ...Args>
ark_internal_function void
Print(char const * const fmt_str, Args... args)
{
    fmt::print(fmt_str, args...);
    fflush(stdout);
}

//------------------------------------------------------------------------------
template <typename ...Args>
ark_internal_function void
Print(String const &fmt_str, Args... args)
{
    fmt::print(fmt_str.CStr(), args...);
    fflush(stdout);
}

//------------------------------------------------------------------------------
template <typename ...Args>
ark_internal_function void
PrintLn(char const * const fmt_str, Args... args)
{
    fmt::print(fmt_str, args...);
    printf("\n");
    fflush(stdout);
}

//------------------------------------------------------------------------------
ark_internal_function void
PrintLn()
{
    PrintLn("");
}

//------------------------------------------------------------------------------
template <typename ...Args>
ark_internal_function void
PrintLn(String const &fmt_str, Args... args)
{
    fmt::print(fmt_str.CStr(), args...);
    printf("\n");
    fflush(stdout);
}

//------------------------------------------------------------------------------
template <typename ...Args>
void
Debug(ark::String const &fmt, Args ... args)
{
    ark::Print("[DEBUG] ");
    ark::PrintLn(fmt, args...);
}

//------------------------------------------------------------------------------
template <typename ...Args>
ark_internal_function void
Fatal(String const &fmt_str, Args... args)
{
    fmt::print("[FATAL] ");
    fmt::print(fmt_str.CStr(), args...);
    printf("\n");
    fflush(stdout);
    ark::Exit(1);
}

//------------------------------------------------------------------------------
template <typename ...Args>
ark_internal_function void
Warn(String const &fmt_str, Args... args)
{
    fmt::print("[WARN] ");
    fmt::print(fmt_str.CStr(), args...);
    printf("\n");
    fflush(stdout);
}

} // namespace ark
