#include "ark_core/Platform/Discovery.hpp"
#if (ARK_CURRENT_OS == ARK_OS_WINDOWS)
// Header
#include "ark_core/Path/PathUtils.hpp"
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/IO.hpp"
#include "ark_core/Math.hpp"
#include "ark_core/Platform.hpp"

// @TODO(stdmatt): Start to use the TranslatePlatformErrorToArk function - Mar 03, 21

namespace ark {

//
//
//
//------------------------------------------------------------------------------
bool
PathUtils::IsAbs(String const& path)
{
    BOOL const result = PathIsRoot(path.CStr());
    return result == TRUE;
}

//
//
//
//------------------------------------------------------------------------------
String
PathUtils::MakeAbsolute(String const &path)
{
    // @todo(stdmatt): Don't hardcode the char-type.       - Jan 02, 2021
    // @todo(stdmatt): Don't hardcode the size of the path - Jan 02, 2021
    // @todo(stdmatt): Better error handling               - Jav 02, 2021
    char buffer[MAX_PATH] = {};
    DWORD const result = GetFullPathName(path.CStr(), MAX_PATH, buffer, nullptr);
    ARK_ASSERT(result != 0, "Failed to GetFullPathName of: ({})", path);

    return String(buffer);
}

//
//
//
//------------------------------------------------------------------------------
String
PathUtils::GetUserHome()
{
    // @todo(stdmatt): Don't hardcode the char-type.        Dec 20, 2020
    // @todo(stdmatt): Don't hardcode the size of the path. Dec 20, 2020
    char path[MAX_PATH];
    HRESULT result = SHGetFolderPathA(nullptr, CSIDL_PROFILE, nullptr, 0, path);

    if(!SUCCEEDED(result)) { // @todo(stdmatt): Handle error... Dec 20, 2020
    }

    return String(path);
}

//------------------------------------------------------------------------------
String
PathUtils::GetDefaultConfigDir()
{
    // @todo(stdmatt): Make it correct....
    // is %appdata% the same concept than the unix /etc and $HOME/.dot_dir
    // (just a random dir name preceded by .) and the macOS ~/Library?
    // or it's "ok" to just create a .dot_folder in the windows home
    // I don't know, it works of course, but I want to know what would be the "envisioned" correct way
    String const prog_name  = Platform::GetProgramName();
    String const config_dir = String::Format("{}{}.{}", GetUserHome(), ark::PATH_SEPARATOR_STR, prog_name);

    return ark::PathUtils::Canonize(config_dir);
}

//------------------------------------------------------------------------------
void
PathUtils::ListEntries(
    String const &path,
    Array<String> *out_files, /*  = nullptr */
    Array<String> *out_dirs   /*  = nullptr */)
{
    if(!out_files && !out_dirs) {
        return;
    }

    // Prepare string for use with FindFile functions.
    // Append '\*' to the directory name.
    ark::String const fullpath = ark::String::Concat(path, "\\*");

    // Find the first file in the directory.
    WIN32_FIND_DATA ffd;
    HANDLE const find_handle = FindFirstFile(fullpath.CStr(), &ffd);
    if(find_handle == INVALID_HANDLE_VALUE) {
        goto _Exit;
    }

    // List all the files in the directory with some info about them.
    do {
        if(CStrEquals(ffd.cFileName, PATH_PARENT_DIRECTORY_STR) ||
           CStrEquals(ffd.cFileName, PATH_CURRENT_DIRECTORY_STR))
        {
            continue;
        }

        // @TODO(stdmatt): Pretty sure that we can use the return value from the Windows API
        // to grab the type of the file, but for now we are going to the easy route - April 28, 21
        String const entry_fullpath = PathUtils::Join(path, ffd.cFileName);
        if(out_dirs && PathUtils::IsDir(entry_fullpath)) {
            out_dirs->PushBack(ffd.cFileName);
        } else if(out_files && PathUtils::IsFile(entry_fullpath)) {
            out_files->PushBack(ffd.cFileName);
        }
    } while(FindNextFile(find_handle, &ffd) != 0);

    if (GetLastError() != ERROR_NO_MORE_FILES) {
        // @todo(stdmatt): Handle errors... - Mar 03, 21
    }

_Exit:
    FindClose(find_handle);
}

//
//
//
//------------------------------------------------------------------------------
Result<bool>
PathUtils::SetAccessTimeUtc(String const &path, time_t const timestamp)
{
    HANDLE const handle = ark::Win32::GetFileHandle(path);
    if(handle == INVALID_HANDLE_VALUE) {
        return Result<bool>::Fail(Platform::GetPlatformLastErrorAsArkError());
    }

    FILETIME const access_time = ark::Win32::POSIX_to_FileTime(timestamp);

    BOOL const ret = SetFileTime(handle, nullptr, &access_time, nullptr);
    if(ret != TRUE) {
        return Result<bool>::Fail(Platform::GetPlatformLastErrorAsArkError());
    }

    return Result<bool>::Success(true);
}

//------------------------------------------------------------------------------
Result<bool>
PathUtils::SetModifiedTimeUtc(String const &path, time_t const timestamp)
{
    HANDLE const handle = ark::Win32::GetFileHandle(path);
    if(handle == INVALID_HANDLE_VALUE) {
        return Result<bool>::Fail(Platform::GetPlatformLastErrorAsArkError());
    }

    FILETIME const modified_time = ark::Win32::POSIX_to_FileTime(timestamp);

    BOOL const ret = SetFileTime(handle, nullptr, nullptr, &modified_time);
    if(ret != TRUE) {
        return Result<bool>::Fail(Platform::GetPlatformLastErrorAsArkError());
    }

    return Result<bool>::Success(true);
}


} // namespace ark
#endif // (ARK_CURRENT_OS == ARK_OS_WINDOWS)
