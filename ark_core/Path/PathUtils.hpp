#pragma once

// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Platform.hpp"
#include "ark_core/Error/Result.hpp"
#include "ark_core/Containers/String.hpp"

namespace ark { namespace PathUtils {

//
// Enums / Typedefs / Constants
//
//------------------------------------------------------------------------------
ARK_STRICT_CONSTEXPR u32 INVALID_TIME = ark::Max<u32>();

//------------------------------------------------------------------------------
// Only takes effect on Windows Platform, ignored on others.
enum class CanonizeCaseOptions {
    DoNotChange,
    ChangeToLowerCase,
    ChangeToUpperCase,
}; // enum class CanonizeCaseOptions

// Only takes effect on Windows Platform, ignored on others.
enum class CanonizeSlashOptions {
    DoNotChange,
    ChangeToForwardSlashes,
    ChangeToBackwardSlashes
}; // enum CanonizeSlashOptions

//------------------------------------------------------------------------------
enum class CreateDirOptions  { NonRecursive, Recursive };
enum class CreateFileOptions { IgnoreIfExists, ErrorIfExists, ForceCreation };

//------------------------------------------------------------------------------
// @todo(stdmatt): Remove the typedefs and just use the types instead... -  March 10, 21
typedef Result<bool> CreateDirResult_t;
typedef Result<bool> CreateFileResult_t;
typedef Result<bool> DeleteDirResult_t;
typedef Result<bool> DeleteFileResult_t;


//------------------------------------------------------------------------------
bool IsAbs (String const &path); // Defined to each platform.
bool IsFile(String const &path); // Common to all platforms.
bool IsDir (String const &path); // Common to all platforms.

//------------------------------------------------------------------------------
String MakeAbsolute(String const &path);                             // Defined to each platform.
String MakeRelative(String const &path, String const &to_what_path); // Common to all platforms.
String Canonize(                                                     // Common to all platforms.
    String               const &path,
    CanonizeCaseOptions  const case_options  = CanonizeCaseOptions ::DoNotChange,
    CanonizeSlashOptions const slash_options = CanonizeSlashOptions::ChangeToForwardSlashes
);

//------------------------------------------------------------------------------
String            GetUserHome        (); // Defined to each platform.
String            GetTempDir         (); // Common to all platforms.
String            GetDefaultConfigDir(); // Defined at each platform.
ARK_INLINE String GetProgramDir      () { return Platform::GetProgramDir(); }
ARK_INLINE String GetCurrentDir      () { return MakeAbsolute(ark::PATH_CURRENT_DIRECTORY_STR); }
ARK_INLINE String GetParentDir       () { return MakeAbsolute(ark::PATH_PARENT_DIRECTORY_STR ); }

//------------------------------------------------------------------------------
void ListEntries(
    String const  &path,
    Array<String> *out_files = nullptr,
    Array<String> *out_dirs  = nullptr); // Defined to each platform.

ARK_INLINE Array<String>
ListDirs(String const &path)
{
    ark::Array<ark::String> entries;
    ListEntries(path, nullptr, &entries);
    return entries;
}

ARK_INLINE Array<String>
ListFiles(String const &path)
{
    ark::Array<ark::String> entries;
    ListEntries(path, &entries, nullptr);
    return entries;
}

//------------------------------------------------------------------------------
time_t GetCreationTimeUtc(String const &path); // Common to all platforms;
time_t GetAccessTimeUtc  (String const &path); // Common to all platforms;
time_t GetModifiedTimeUtc(String const &path); // Common to all platforms;

Result<bool> SetAccessTimeUtc  (String const &path, time_t const timestamp); // Defined to each platform.
Result<bool> SetModifiedTimeUtc(String const &path, time_t const timestamp); // Defined to each platform.

//------------------------------------------------------------------------------
size_t GetFileSize(String const &path);

//------------------------------------------------------------------------------
String Dirname (String const &path); // Common to all platforms.
String Basename(String const &path); // Common to all platforms.

//------------------------------------------------------------------------------
template <typename ...Args>
String
Join(String const &str, Args... args)
{
    String result = str;
    if(!result.IsEmptyOrWhitespace()){
         result.Append(ark::PATH_SEPARATOR_CHAR);
    }

    int unpack[]{0, (
        result += ToString(args) + ark::PATH_SEPARATOR_CHAR,
    0)...};
    static_cast<void>(unpack);

    result.PopBack();
    return result;
};


//------------------------------------------------------------------------------
ARK_INLINE String // @todo(stdmatt): Move to implemenation file. - Feb 11, 21
RemoveExtension(String const &filename)
{
    size_t      const last_dot_index = filename.FindLastIndexOf(".");
    ark::String const clean_filename = filename.SubString(0, last_dot_index);
    return clean_filename;
}

//------------------------------------------------------------------------------
ARK_INLINE String
ChangeExtension(String const &filename, String const &new_extension)
{
    String new_filename = RemoveExtension(filename);
    new_filename.Concat(new_extension);

    return new_filename;
}

//------------------------------------------------------------------------------
CreateDirResult_t  CreateDir (String const &path,     CreateDirOptions  const options); // Defined to all platforms
CreateFileResult_t CreateFile(String const &filename, CreateFileOptions const options); // Defined to all platforms
DeleteDirResult_t  DeleteDir (String const &path    );                                  // Defined to all platforms
DeleteFileResult_t DeleteFile(String const &filename);                                  // Defined to all platforms

} // namespace PathUtils
} // namespace ark
