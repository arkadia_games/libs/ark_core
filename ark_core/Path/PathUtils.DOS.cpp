#include "ark_core/Platform/Discovery.hpp"
#if (ARK_CURRENT_OS == ARK_OS_DOS)
// Header
#include "ark_core/Path/PathUtils.hpp"
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/IO.hpp"
#include "ark_core/Math.hpp"
#include "ark_core/Platform.hpp"
#include "ark_core/Memory.hpp"

namespace ark {

//------------------------------------------------------------------------------
bool
PathUtils::IsAbs(String const& path)
{
    // On unix is easy... Just check if path is rooted.
    return path.StartsWith(ark::PATH_SEPARATOR_STR);
}

//------------------------------------------------------------------------------
String
PathUtils::MakeAbsolute(String const &path)
{
    if(PathUtils::IsAbs(path)) {
        return path;
    }

    char *resolved_path = realpath(path.CStr(), nullptr);
    if(!resolved_path) {
        return "";
    }

    ark::String return_path = String(resolved_path);
    ark::FreeMemory(resolved_path);

    return return_path;
}

//
//
//
//------------------------------------------------------------------------------
String
PathUtils::GetUserHome()
{
    // @todo(stdmatt): Add possibility to query the home of any user... Jan 30, 21
    //
    // Try first get the Environment variable.
    char const * const home_env = getenv("HOME");
    if(home_env) {
        return home_env;
    }

    // If it fails try to get the userhome.
    auto _uid    = getuid  ();
    auto _passwd = getpwuid(_uid);
    if(_passwd) {
        return _passwd->pw_dir;
    }

    // Failed to retrieve the home path both
    // from environment var and passwd database.
    return String::Empty();
}

//------------------------------------------------------------------------------
String
PathUtils::GetDefaultConfigDir()
{
    String const prog_name  = Platform::GetProgramName();
    String const config_dir = String::Format("{}{}.{}", GetUserHome(), ark::PATH_SEPARATOR_STR, prog_name);

    return ark::PathUtils::Canonize(config_dir);
}

#include <dirent.h>

//------------------------------------------------------------------------------
void
PathUtils::ListEntries(
    String const  &path,
    Array<String> *out_files, /*  = nullptr */
    Array<String> *out_dirs   /*  = nullptr */)
{
    if(!out_files && !out_dirs) {
        return;
    }

    DIR           *_dir       = nullptr;
    struct dirent *_dir_entry = nullptr;
    _dir = opendir(path.CStr());
    if(!_dir) {
        return;
    }

    while(true) {
        _dir_entry = readdir(_dir);
        if(!_dir_entry) {
            goto _Exit;
        }

        if(CStrEquals(_dir_entry->d_name, PATH_PARENT_DIRECTORY_STR) ||
           CStrEquals(_dir_entry->d_name, PATH_CURRENT_DIRECTORY_STR))
        {
            continue;
        }

        String const entry_fullpath = PathUtils::Join(path, _dir_entry->d_name);
        if(out_dirs && PathUtils::IsDir(entry_fullpath)) {
            out_dirs->PushBack(_dir_entry->d_name);
        } else if(out_files && PathUtils::IsFile(entry_fullpath)) {
            out_files->PushBack(_dir_entry->d_name);
        }
    }

_Exit:
    closedir(_dir);
}


//
//
//
//------------------------------------------------------------------------------
Result<bool>
PathUtils::SetAccessTimeUtc(String const &path, time_t const timestamp)
{
    // @todo(stdmatt): Implement - April 01, 2020
    return Result<bool>::Success(true);
}

//------------------------------------------------------------------------------
Result<bool>
PathUtils::SetModifiedTimeUtc(String const &path, time_t const timestamp)
{

    // @todo(stdmatt): Implement - April 01, 2020
    return Result<bool>::Success(true);
}

} // namespace ark
#endif // (ARK_CURRENT_OS == ARK_OS_GNU)
