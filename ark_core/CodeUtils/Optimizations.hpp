﻿#pragma once
// Arkadia
#include "ark_core/Platform/Discovery.hpp"

//----------------------------------------------------------------------------//
// Optimizations                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
#if (ARK_COMPILER_IS_CLANG || ARK_COMPILER_IS_GCC)
    #define ARK_OPTIMIZE_OFF _Pragma("clang optimize off")
    #define ARK_OPTIMIZE_ON  _Pragma("clang optimize on")
#elif (ARK_COMPILER_IS_MSVC)
    #define ARK_OPTIMIZE_OFF __pragma(optimize("", off))
    #define ARK_OPTIMIZE_ON  __pragma(optimize("", on ))
#else
    #error "Compiler not supported...";
#endif
