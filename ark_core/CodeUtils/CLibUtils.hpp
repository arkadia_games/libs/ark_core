﻿#pragma once

// Arkadia
#include "ark_core/CodeUtils/NumericTypes.hpp"
#include "ark_core/Platform/PlatformIncludes.hpp"

namespace ark {

typedef time_t time_t;
typedef tm     tm_t;

constexpr auto DEFAULT_MKDIR_MODE = 0700;

tm_t*  gmtime(time_t const * const _time);
i32    mkdir (char   const * const path, u32 const mode = DEFAULT_MKDIR_MODE);
FILE*  fopen (char   const * const path, char const * const mode);
i32    open  (char   const * const filename, i32 const flags, i32 const mode);
i32    close (i32    const         fd);

}
