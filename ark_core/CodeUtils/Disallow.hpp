﻿#pragma once

//----------------------------------------------------------------------------//
// Disallow                                                                   //
//----------------------------------------------------------------------------//
//
// CTOR / DTOR
//
///-----------------------------------------------------------------------------
/// @brief Makes the constructor deleted.
#define ARK_DISALLOW_CTOR(_type_)  _type_() = delete;

///-----------------------------------------------------------------------------
/// @brief Makes the destructor deleted.
#define ARK_DISALLOW_DTOR(_type_) ~_type_() = delete;

///-----------------------------------------------------------------------------
/// @brief Makes both the constructor and destructor deleted.
#define ARK_DISALLOW_CTOR_DTOR(_type_) \
    ARK_DISALLOW_CTOR(_type_)          \
    ARK_DISALLOW_DTOR(_type_)

//
// Copy
//
///-----------------------------------------------------------------------------
/// @brief Makes the copy-constructor deleted.
#define ARK_DISALLOW_COPY_CTOR(_type_) \
    _type_(const _type_ &) = delete;

///-----------------------------------------------------------------------------
/// @brief Makes the copy-assign operator deleted.
#define ARK_DISALLOW_COPY_ASSIGN(_type_) \
    _type_& operator=(const _type_&) = delete;

///-----------------------------------------------------------------------------
/// @brief Makes both the copy-constructor and copy-assign operator deleted.
#define ARK_DISALLOW_COPY_CTOR_AND_COPY_ASSIGN(_type_) \
    ARK_DISALLOW_COPY_CTOR(_type_)                     \
    ARK_DISALLOW_COPY_ASSIGN(_type_)

//
// Move
//
///-----------------------------------------------------------------------------
/// @brief Makes the move-constructor deleted.
#define ARK_DISALLOW_MOVE_CTOR(_type_) \
    _type_(const _type_ &&) = delete;

///-----------------------------------------------------------------------------
/// @brief Makes the move-assign operator deleted.
#define ARK_DISALLOW_MOVE_ASSIGN(_type_) \
    _type_& operator=(const _type_&&) = delete;

///-----------------------------------------------------------------------------
/// @brief Makes both the move-constructor and move-assign operator deleted.
#define ARK_DISALLOW_MOVE_CTOR_AND_MOVE_ASSIGN(_type_) \
    ARK_DISALLOW_MOVE_CTOR(_type_)                     \
    ARK_DISALLOW_MOVE_ASSIGN(_type_)

//
// Everything
//
///-----------------------------------------------------------------------------
/// @brief
///    Makes the constructor, desctructor, copy-ctor, copy-assign,
///    move-ctor and move-assign deleted.
#define ARK_DISALLOW_EVERYTHING(_type_)            \
    ARK_DISALLOW_CTOR_DTOR(_type_)                 \
    ARK_DISALLOW_COPY_CTOR_AND_COPY_ASSIGN(_type_) \
    ARK_DISALLOW_MOVE_CTOR_AND_MOVE_ASSIGN(_type_)

#define ARK_STATIC_CLASS(_type_) \
        ARK_DISALLOW_EVERYTHING(_type_)

#define ARK_DISALLOW_COPY(_type_) \
        ARK_DISALLOW_COPY_CTOR_AND_COPY_ASSIGN(_type_)

#define ARK_DISALLOW_MOVE(_type_) \
        ARK_DISALLOW_MOVE_CTOR_AND_MOVE_ASSIGN(_type_)

#define ARK_DISALLOW_COPY_AND_MOVE(_type_) \
        ARK_DISALLOW_COPY(_type_)          \
        ARK_DISALLOW_MOVE(_type_)
