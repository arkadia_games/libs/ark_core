﻿// Header
#include "ark_core/CodeUtils/CLibUtils.hpp"
// Ardkadia
#include "ark_core/Platform.hpp"

// Windows have a lot of complaining regarding the libc functions.
// But we want to continue to use them as it as defined on GNU/Linux,
// so just create wrappers around it...
#if ARK_OS_IS_WINDOWS

namespace ark {
    ark::tm_t g_Global_gmtime_var;
} // namespace ark;

//------------------------------------------------------------------------------
ark::tm_t*
ark::gmtime(ark::time_t const * const _time)
{
    errno_t error = ::gmtime_s(&g_Global_gmtime_var, _time);
    // @todo(stdmatt): Implement error handling... Mach 17, 2021
    return &g_Global_gmtime_var;
}

//------------------------------------------------------------------------------
i32
ark::mkdir(char const * const path, const u32 mode)
{
    ARK_UNUSED(mode);
    return ::_mkdir(path);
}

//------------------------------------------------------------------------------
FILE* ark::fopen(char const * const path, char const * const mode)
{
    FILE *file = nullptr;
    errno_t result = fopen_s(&file, path, mode);
    // @todo(stdmatt): Check result - April 17, 2021
    return file;
}

//------------------------------------------------------------------------------
i32 ark::open(char const * const filename, i32 const flags, i32 const mode)
{
    // @BUG(stdmatt): It's creating the thing as readonly... April 27, 21
    i32 fd = ::_open(filename, flags);
    return fd;
}

//------------------------------------------------------------------------------
i32 ark::close(i32 const fd)
{
    return ::_close(fd);
}


// ARK_OS_IS_WINDOWS == FALSE
#else

//------------------------------------------------------------------------------
ark::tm_t* ark::gmtime(ark::time_t const * const _time)                                { return ::gmtime(_time);     }
i32        ark::mkdir (char const * const path, const u32 mode)                        { return ::mkdir(path, mode); }
FILE*      ark::fopen (char const * const path, char const * const mode)               { return ::fopen(path, mode); }
i32        ark::open  (char const * const filename, i32 const flags, i32 const mode) { return ::open(filename, flags, mode); }
i32        ark::close (i32  const         fd)                                        { return ::close(fd); }
#endif
