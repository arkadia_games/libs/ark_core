﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils/Inline.hpp"
#include "ark_core/CodeUtils/Constexpr.hpp"

//----------------------------------------------------------------------------//
// Casts                                                                      //
//----------------------------------------------------------------------------//
template <typename To_Type, typename From_Type>
ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
To_Type Cast(From_Type value)
{
    return (To_Type)(value);
}
