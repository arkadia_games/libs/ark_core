﻿#pragma once
// Arkadia
#include "ark_core/Platform/Discovery.hpp"

//----------------------------------------------------------------------------//
// Inline                                                                     //
//----------------------------------------------------------------------------//
#define ARK_INLINE inline

#if (ARK_COMPILER_IS_CLANG || ARK_COMPILER_IS_GCC)
    #define ARK_FORCE_INLINE    inline
    #define ARK_FORCE_NO_INLINE
#elif (ARK_COMPILER_IS_MSVC)
    #define ARK_FORCE_INLINE    __forceinline
    #define ARK_FORCE_NO_INLINE _declspec(noinline)
#else
    #error "Compiler not supported...";
#endif
