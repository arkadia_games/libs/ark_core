﻿#pragma once

// Arkadia
#include "ark_core/CodeUtils/Constexpr.hpp"

namespace ark  {

ARK_STRICT_CONSTEXPR char NULL_CHAR = '\0';

//------------------------------------------------------------------------------
#if (ARK_CURRENT_OS == ARK_OS_WINDOWS)
    ARK_STRICT_CONSTEXPR char NEW_LINE_CHAR                  =  '\n';
    ARK_STRICT_CONSTEXPR char PATH_SEPARATOR_CHAR            =  '/' ;
    ARK_STRICT_CONSTEXPR char PATH_ALTERNATE_SEPARATOR_CHAR  =  '\\';
#else // (ARK_CURRENT_OS == ARK_OS_WINDOWS)
    ARK_STRICT_CONSTEXPR char NEW_LINE_CHAR                  =  '\n';
    ARK_STRICT_CONSTEXPR char PATH_SEPARATOR_CHAR            =  '/' ;
    ARK_STRICT_CONSTEXPR char PATH_ALTERNATE_SEPARATOR_CHAR  =  '/' ;
#endif // (ARK_CURRENT_OS == ARK_OS_WINDOWS)

//------------------------------------------------------------------------------
ARK_STRICT_CONSTEXPR char NEW_LINE_STR                [2] = { NEW_LINE_CHAR                , NULL_CHAR };
ARK_STRICT_CONSTEXPR char PATH_SEPARATOR_STR          [2] = { PATH_SEPARATOR_CHAR          , NULL_CHAR };
ARK_STRICT_CONSTEXPR char PATH_ALTERNATE_SEPARATOR_STR[2] = { PATH_ALTERNATE_SEPARATOR_CHAR, NULL_CHAR };
ARK_STRICT_CONSTEXPR auto PATH_CURRENT_DIRECTORY_STR      =  ".";
ARK_STRICT_CONSTEXPR auto PATH_PARENT_DIRECTORY_STR       = "..";

} // namespace ark
