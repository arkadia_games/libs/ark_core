﻿#pragma once
// Arkadia
#include "ark_core/Platform/Discovery.hpp"

//----------------------------------------------------------------------------//
// Constexpr                                                                  //
//----------------------------------------------------------------------------//
// @todo(stdmatt): Check if the c++ version supports constexpr - Jan 16, 21
#define ARK_STRICT_CONSTEXPR constexpr
#define ARK_LOOSE_CONSTEXPR  constexpr
