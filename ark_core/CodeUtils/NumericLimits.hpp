﻿#pragma once
// std
#include <limits>
// Arkadia
#include "ark_core/CodeUtils/Constexpr.hpp"
#include "ark_core/Debug/Assert.hpp"
#include "ark_core/Platform/PlatformIncludes.hpp"

namespace ark {

//------------------------------------------------------------------------------
template <typename T>
struct NumericLimits
    : public std::numeric_limits<T>
{
    static constexpr T Max = std::numeric_limits<T>::max();
    static constexpr T Min = std::numeric_limits<T>::max();
};

template <typename T> ARK_LOOSE_CONSTEXPR static T Max() { return NumericLimits<T>::Max; }
template <typename T> ARK_LOOSE_CONSTEXPR static T Min() { return NumericLimits<T>::Min; }

//
// Data Types
//
//------------------------------------------------------------------------------
template <typename T>
struct MinMax {
    T min;
    T max;

    ARK_STRICT_CONSTEXPR ARK_FORCE_INLINE
    MinMax(T min, T max)
        : min(min)
        , max(max)
    {
        ARK_ASSERT(min <= max, "min needs to <= max - min({}), max({})", min, max);
    }

    ARK_STRICT_CONSTEXPR ARK_FORCE_INLINE
    T Normalized(T const &v) const
    {
        return (v - min) / (max - min);
    }

    ARK_STRICT_CONSTEXPR ARK_FORCE_INLINE
    T Lerp(f32 const t) const
    {
        return ((1.0f - t) * min) + (t * max);
    }

    ARK_STRICT_CONSTEXPR ARK_FORCE_INLINE
    T Clamp(T const value) const
    {
        if(value < min) { return min; }
        if(value > max) { return max; }
        return value;
    }
};

} // namespace ark
