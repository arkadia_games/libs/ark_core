#pragma once
// std
#include <functional>
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Error.hpp"
#include "ark_core/Containers/String.hpp"

namespace ark { namespace CMD {

// @todo(stdmatt): Parser is having a big problem when tried to parse this command line option.
// I assume that because we were naive and sloppy when tried to parse the - character
// to decide if was a argument or not...
//
// Should not be difficulty to fix, but needs to be done :( - Feb 08, 21
//         "2021-02-07 02_16_22-(1146) Pixel Art Class - Tile Set Art - YouTube.png"


//
//
//
void   Set(i32    const argc, char const *argv[]);
void   Set(String const &str);
String Get();

//
// Command Line Parser
//
class Argument
{
    //
    // Enums / Constants / Typedefs
    //
public:
    //--------------------------------------------------------------------------
    enum class ParseStatus {
        Valid, Invalid
    }; // enum ParseStatus

    //--------------------------------------------------------------------------
    typedef std::function<ParseStatus(String const &)> ArgumentParseCallback_t;
    typedef MinMax<size_t>                             MinMax_t;

    //--------------------------------------------------------------------------
    static const MinMax_t RequiresNoValues;
    static const MinMax_t RequiresOneValue;
    static const MinMax_t OptionalOneValue;
    //
    // CTOR / DTOR
    //
public:
    //--------------------------------------------------------------------------
    Argument(
        String                  const &short_name,
        String                  const &long_name,
        String                  const &description,
        MinMax_t                const &values_requirement,
        ArgumentParseCallback_t const &parse_callback);

    //
    // Public Methods
    //
public:
    //--------------------------------------------------------------------------
    String const & GetShortName  () const { return _short_name;  }
    String const & GetLongName   () const { return _long_name;   }
    String const & GetDescription() const { return _description; }

    //--------------------------------------------------------------------------
    Array<String> const & GetValues          () const { return _values;             }
    bool                  HasValues          () const { return !_values.IsEmpty();  }
    size_t                ValuesCount        () const { return _values.Count();     }
    MinMax_t const &      GetValueRequirement() const { return _values_requirement; }

    //--------------------------------------------------------------------------
    String const &
    GetFirstValue(String const &default_value = String::Empty()) const
    {
        if(HasValues()) {
            return GetValues().Front();
        }
        return default_value;
    }

    //--------------------------------------------------------------------------
    void SetAsFound()
    {


        _found = true;
    }
    bool WasFound  () const  { return _found; }

    //--------------------------------------------------------------------------
    ARK_INLINE ParseStatus
    ParseArgument(String const &value = String::Empty())
    {
        ParseStatus status = ParseStatus::Valid;
        if(_parse_callback) {
            status = _parse_callback(value);
        }
        if(status == ParseStatus::Valid && !value.IsEmptyOrWhitespace()) {
            _values.PushBack(value);
        }

        return status;
    }

    //
    // iVars
    //
private:
    String _short_name;
    String _long_name;
    String _description;

    Array<String>           _values;
    MinMax_t                _values_requirement;
    ArgumentParseCallback_t _parse_callback;

    bool _found = false;
}; // class Argument


class Parser
{
    ARK_DISALLOW_COPY(Parser);

    //
    // Enums / Constants / Typedefs
    //
public:
    //--------------------------------------------------------------------------
    enum class ErrorCodes {
        INVALID_FLAG         = 1,
        NOT_ENOUGH_ARGUMENTS = 2,
        FAILED_ON_PARSE      = 3,
    };

    enum class ErrorHandling {
        DIE_ON_ERROR,
        RETURN_ON_ERROR
    };

    //--------------------------------------------------------------------------
    static String const ShortFlagPrefix;
    static String const LongFlagPrefix;
    //--------------------------------------------------------------------------
    static char const LongFlagSeparator;

    //--------------------------------------------------------------------------
    typedef Result<bool> ParseResult_t;

    //
    // CTOR / DTOR
    //
public:
    //--------------------------------------------------------------------------
    explicit Parser(                                     ErrorHandling const error_handling = ErrorHandling::DIE_ON_ERROR);
    explicit Parser(String const &cmd_line_str,          ErrorHandling const error_handling = ErrorHandling::DIE_ON_ERROR);
    explicit Parser(i32 argc, char const *argv[],        ErrorHandling const error_handling = ErrorHandling::DIE_ON_ERROR);
    explicit Parser(Array<String> const &cmd_line_items, ErrorHandling const error_handling = ErrorHandling::DIE_ON_ERROR);

    ~Parser();

    //
    // Public Methods
    //
public:
    //--------------------------------------------------------------------------
    Argument* CreateArgument(
        String                            const &short_name,
        String                            const &long_name,
        String                            const &description,
        Argument::MinMax_t                const &values_requirements = Argument::RequiresNoValues,
        Argument::ArgumentParseCallback_t const &parse_callback      = nullptr);


    //--------------------------------------------------------------------------
    Argument const * FindArgumentByName(String const &name) const;
    Argument       * FindArgumentByName(String const &name) {
        Argument const * const arg = Cast<const Parser*>(this)->FindArgumentByName(name);
        return Cast<Argument *>(arg);
    }

    //--------------------------------------------------------------------------
    Array<Argument*> const & GetArguments       () const { return _arguments;         }
    Array<String>    const & GetPositionalValues() const { return _positional_values; }

    //--------------------------------------------------------------------------
    ParseResult_t Evaluate();

    //--------------------------------------------------------------------------
    ark::String GenerateHelpString(bool const should_sort_args = false);

    //
    // Private Methods
    //
private:
    //--------------------------------------------------------------------------
    bool IsShortFlag(String const &item) const;
    bool IsLongFlag (String const &item) const;
    bool IsFlag     (String const &item) const { return IsShortFlag(item) || IsLongFlag(item); }

    //--------------------------------------------------------------------------
    Array<String> SplitShortFlag(String const &item) const;
    Array<String> SplitLongFlag (String const &item) const;

    ParseResult_t CreateFailResultOrDie(ErrorCodes code, ark::String const &msg) const;

    //
    // iVars
    //
private:
    Array<Argument *> _arguments;
    Array<String>     _positional_values;
    Array<String>     _cmd_line;
    ErrorHandling     _error_handling;
}; // class Parser


} // namespace CMD
} // namespace ark
