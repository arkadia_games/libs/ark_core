// Header
#include "ark_core/Terminal/CMD.hpp"
// Arkadia
#include "ark_core/IO.hpp"
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Math.hpp"
#include "ark_core/IO.hpp"

namespace ark {

//
// Globals
//
//------------------------------------------------------------------------------
ark_global_var ark::String g_CommandLine;

//
//
//
//------------------------------------------------------------------------------
void
CMD::Set(i32 const argc, char const *argv[])
{
    g_CommandLine.Clear();
    size_t size = argc;
    for(i32 i = 0; i < argc; ++i) {
        size += CStrLen(argv[i]);
    }

    g_CommandLine.Reserve(size);
    for(i32 i = 0; i < argc; ++i) {
        g_CommandLine.Append(argv[i]);
        g_CommandLine.Append(" ");
    }
}

//------------------------------------------------------------------------------
void
CMD::Set(String const &str)
{
    g_CommandLine = str;
}

//------------------------------------------------------------------------------
String
CMD::Get()
{
    return g_CommandLine;
}


//
// Argument
//

//------------------------------------------------------------------------------
const CMD::Argument::MinMax_t CMD::Argument::RequiresNoValues = {0, 0};
const CMD::Argument::MinMax_t CMD::Argument::RequiresOneValue = {1, 1};
const CMD::Argument::MinMax_t CMD::Argument::OptionalOneValue = {0, 1};

//------------------------------------------------------------------------------
CMD::Argument::Argument(
    String                  const &short_name,
    String                  const &long_name,
    String                  const &description,
    MinMax_t                const &values_requirement,
    ArgumentParseCallback_t const &parse_callback)
    : _short_name        (short_name)
    , _long_name         (long_name)
    , _description       (description)
    , _values_requirement(values_requirement)
    , _parse_callback    (parse_callback)
{
    ARK_ASSERT(
        !(short_name.IsEmptyOrWhitespace() && long_name.IsEmptyOrWhitespace()),
        "Argument can't have both short and long names empty"
    );
}

//
// Parser
//
//------------------------------------------------------------------------------
String const CMD::Parser::ShortFlagPrefix   = "-";
String const CMD::Parser::LongFlagPrefix    = "--";

//------------------------------------------------------------------------------
char const CMD::Parser::LongFlagSeparator    = '=';


//------------------------------------------------------------------------------
CMD::Parser::Parser(ErrorHandling const error_handling)
    : Parser(g_CommandLine, error_handling)
{
    // Empty...
}

//------------------------------------------------------------------------------
CMD::Parser::Parser(String const &cmd_line_str, ErrorHandling const error_handling)
    : _cmd_line(cmd_line_str.Split(' '))
    , _error_handling(error_handling)
{
   // Empty...
}

//------------------------------------------------------------------------------
CMD::Parser::Parser(i32 argc, char const *argv[], ErrorHandling const error_handling)
    : _error_handling(error_handling)
{
    _cmd_line.Reserve(argc);
    for(i32 i = 0; i < argc; ++i) {
        _cmd_line.PushBack(argv[i]);
    }
}

//------------------------------------------------------------------------------
CMD::Parser::Parser(Array<String> const &cmd_line_items, ErrorHandling const error_handling)
    : _cmd_line(cmd_line_items)
    , _error_handling(error_handling)
{
    // Empty...
}

//------------------------------------------------------------------------------
CMD::Parser::~Parser()
{
    for(auto *arg : _arguments) {
        delete arg;
    }
}



//------------------------------------------------------------------------------
CMD::Argument *
CMD::Parser::CreateArgument(
    String                            const &short_name,
    String                            const &long_name,
    String                            const &description,
    Argument::MinMax_t                const &values_requirements,
    Argument::ArgumentParseCallback_t const &parse_callback)
{
    ARK_ASSERT(
        !(short_name.IsEmptyOrWhitespace() && long_name.IsEmptyOrWhitespace()),
        "Argument can't have both short and long names empty"
    );

    // Try to find it before...
    Argument *arg = short_name.IsEmpty() ? nullptr : FindArgumentByName(short_name);
    if(arg) {
        return arg;
    }

    arg = long_name.IsEmpty() ? nullptr : FindArgumentByName(long_name);
    if(arg) {
        return arg;
    }

    // Ok we don't have it, so let's create.
    arg = new Argument(
        short_name,
        long_name,
        description,
        values_requirements,
        parse_callback
    );
    _arguments.PushBack(arg);

    return arg;
}

//------------------------------------------------------------------------------
CMD::Argument const *
CMD::Parser::FindArgumentByName(String const &name) const
{
    String clean_name = name;
    clean_name.TrimLeft(Parser::ShortFlagPrefix[0]);

    for(auto *arg : _arguments) {
        if(arg->GetShortName() == clean_name || arg->GetLongName() == clean_name) {
            return arg;
        }
    }
    return nullptr;
}

//------------------------------------------------------------------------------
CMD::Parser::ParseResult_t
CMD::Parser::Evaluate()
{
    Array<String> clean_components;
    clean_components.Reserve(_cmd_line.Count());
    // Clean up the components of the command line.
    //   This will make ALL the items to be in a different string.
    //   For example:
    //      prog -abc          -> prog -a -b -c
    //      prog --flag= value -> prog --flag value
    for(
        size_t i = 0,
        cmd_line_count = _cmd_line.Count();
        i < cmd_line_count;
        ++i)
    {
        String const &item = _cmd_line[i];
        if(IsShortFlag(item)) {
            Array<String> split_flags = SplitShortFlag(item);
            clean_components.PushBack(split_flags);
        }
        else if(IsLongFlag(item)) {
            Array<String> split_flags = SplitLongFlag(item);
            clean_components.PushBack(split_flags);
        }
        else {
            clean_components.PushBack(item);
        }
    }

    //
    for(
        size_t i = 1, // 0 is the name of the program
        components_count = clean_components.Count();
        i < components_count;
        ++i)
    {
        String const &item    = clean_components[i];
        bool   const  is_flag = IsShortFlag(item) || IsLongFlag(item);

        Argument *arg = FindArgumentByName(item);
        //
        // Pure Positional
        if(!arg && !is_flag) {
            _positional_values.PushBack(item);
            continue;
        }

        //
        // Invalid flag :(
        if(!arg && is_flag) {
            return CreateFailResultOrDie(
                ErrorCodes::INVALID_FLAG,
                String::Format("({}) is not a valid flag.", item)
            );
        }

        //
        // Argument ;D
        arg->SetAsFound();
        Argument::MinMax_t const &value_requirement = arg->GetValueRequirement();

        // Does not require any value...
        if(value_requirement.min == 0 &&
           value_requirement.max == 0)
        {
            arg->ParseArgument();
        }
        // Does require an value...
        else
        {
            do {
                size_t const next_index   = (i + 1);
                size_t const values_count = arg->ValuesCount();

                // Command line ended without providing enough arguments to this flag.
                if(next_index >= components_count && values_count < value_requirement.min) {
                    return CreateFailResultOrDie(
                        ErrorCodes::NOT_ENOUGH_ARGUMENTS,
                        String::Format(
                            "Flag ({}) requires at least ({}) values - Found: ({})",
                            item,
                            value_requirement.min,
                            values_count
                        )
                    );
                }

                if(next_index >= components_count) {
                    break;
                }

                String const &next_item = clean_components[next_index];
                // Found another flag but we didn't provide enough arguments to this flag.
                if(IsFlag(next_item) && values_count < value_requirement.min) {
                    return CreateFailResultOrDie(
                        ErrorCodes::NOT_ENOUGH_ARGUMENTS,
                        String::Format(
                            "Flag ({}) requires at least ({}) values - Found: ({})",
                            item,
                            value_requirement.min,
                            values_count
                        )
                    );
                }

                Argument::ParseStatus const status = arg->ParseArgument(next_item);
                if(status != Argument::ParseStatus::Valid) {
                    return CreateFailResultOrDie(
                        ErrorCodes::FAILED_ON_PARSE,
                        String::Format(
                            "Failed to parse Flag ({}) with value ({})",
                            item,
                            next_item
                        )
                    );
                }

                i = next_index;
                // Already parsed enough values for this flag...
                if(arg->ValuesCount() >= value_requirement.max) {
                    break;
                }
            } while(true);
        }
    }

    return ParseResult_t::Success(true);
}

//--------------------------------------------------------------------------
ark::String
CMD::Parser::GenerateHelpString(bool const should_sort_args)
{
    // @todo(stdmatt): Check what happens when we have big description? April 02, 2021
    // @todo(stdmatt): A LOT of string concats... we must have a sort of string builder or so - april 02, 2021

    ark::String str;
    std::sort(_arguments.begin(), _arguments.end(), [should_sort_args](Argument const * const a1, Argument const * const a2) {
        if(!should_sort_args) {
            return false;
        }
        ark::String const &a1_short = a1->GetShortName();
        ark::String const &a2_short = a2->GetShortName();
        if(!a1_short.IsEmpty() && !a2_short.IsEmpty()) {
            return a1_short < a2_short;
        }

        ark::String const &a1_long = a1->GetLongName();
        ark::String const &a2_long = a2->GetLongName();
        if(!a1_short.IsEmpty() && !a2_long.IsEmpty()) {
            return a1_short[0] < a2_long[0];
        }
        if(a1_short.IsEmpty() && !a2_short.IsEmpty()) {
            return a1_long[0] < a2_short[0];
        }
        if(a1_short.IsEmpty() && a2_short.IsEmpty()) {
            return a1_long < a2_long;
        }
        return false;
    });

    auto CleanShort = [](ark::String const &str) {
        if(str.IsEmpty()) { return String("    "); }
        return String::Format("-{}, ", str);
    };

    auto CleanLong = [](ark::String const &str) {
        if(str.IsEmpty()) { return String("   "); }
        return String::Format("--{}", str);
    };

    auto BuildParam = [](Argument const * const arg) {
        Argument::MinMax_t const &req = arg->GetValueRequirement();
        if(req.min == 0 && req.max == 0) {
            return String::Empty();
        }
        if(req.min == 0 && req.max == 1) {
            return String("[arg]");
        }
        if(req.min == 0 && req.max > 1) {
            return String("[arg...]");
        }
        if(req.min == 1 && req.max == 1) {
            return String("<arg>");
        }
        if(req.min == 1 && req.max > 1) {
            return String("<arg...>");
        }
        return String::Empty();
    };

    ark::Array<ark::String> args_lines;
    ark::Array<ark::String> params_lines;
    size_t longest_arg  = 0;
    size_t longest_param= 0;
    for(Argument const * const arg : _arguments) {
        ark::String flag_str = String::Format(
            "{} {}",
            CleanShort(arg->GetShortName()),
            CleanLong (arg->GetLongName ())
        );
        ark::String param_str = BuildParam(arg);

        args_lines  .PushBack(flag_str);
        params_lines.PushBack(param_str);

        longest_arg   = ark::Max(longest_arg,   flag_str .Length());
        longest_param = ark::Max(longest_param, param_str.Length());
    }
    longest_arg   += 1;
    longest_param += 1;

    str += ark::Platform::GetProgramName() + "\n";
    for(size_t i = 0; i < args_lines.Count(); ++i) {
        Argument const * const arg = _arguments[i];
        ark::String const &arg_line   = args_lines  [i];
        ark::String const &param_line = params_lines[i];

        str += String::Format("  {:<{}}", arg_line.CStr(), longest_arg);
        str += String::Format("{:<{}}", param_line.CStr(), longest_param);
        str += String::Format(": {}\n", arg->GetDescription().CStr());
    }
    str += String::Format("Notes:\n  {}", "Mandatory arguments to long options are mandatory for short options too.");
    return str;
}

//------------------------------------------------------------------------------
bool
CMD::Parser::IsShortFlag(String const &item) const
{
    if(item.IsEmptyOrWhitespace()) {
        return false;
    }
    size_t const len = item.Length();
    if(len < 2) {
        return false;
    }

    if(item.StartsWith(Parser::LongFlagPrefix)) {
        return false;
    }

    return item.StartsWith(Parser::ShortFlagPrefix);
}

//------------------------------------------------------------------------------
bool
CMD::Parser::IsLongFlag(String const &item) const
{
   if(IsShortFlag(item)) {
       return false;
   }

    if(item.StartsWith(Parser::LongFlagPrefix)) {
        return true;
    }

    return false;
}

//------------------------------------------------------------------------------
Array<String>
CMD::Parser::SplitShortFlag(String const &item) const
{
    // Given the arguments:
    //    prog -abc
    // We can split the flag in two different ways.
    //    1) prog -a -b -c  : Each char becomes a flag.
    //    2) prog -a bc     : -a is a flag and bc is the value of it.
    // To decide we need to check the current Arguments and see if
    // we have an argument with that name, if so it's a flag
    // otherwise it's a value.
    Array<String> split_items;
    for(size_t i = 1, item_len = item.Length(); i < item_len; ++i) {
        // @todo(stdmatt): O(n^2) just to find the flags, improve it - Dec 31, 2020.
        // @todo(stdmatt): Creating a string just to pass a char - Dec 31, 2020.
        char const c = item[i];
        Argument const * const arg = FindArgumentByName(String(c));
        if(arg) { // This is a flag...
            split_items.PushBack(String::Format("-{}", c));
        } else {
            split_items.PushBack(String(c));
        }
    }

    return split_items;
}

//------------------------------------------------------------------------------
Array<String>
CMD::Parser::SplitLongFlag(String const &item) const
{
    Array<String> split_items =  item.Split(Parser::LongFlagSeparator);
    return split_items;
}

//------------------------------------------------------------------------------
CMD::Parser::ParseResult_t
CMD::Parser::CreateFailResultOrDie(ErrorCodes code, ark::String const &msg) const
{
    if(_error_handling == ErrorHandling::DIE_ON_ERROR) {
        ark::PrintLn("{} - Aborting...", msg);
        ark::Exit(1);
    }

    ParseResult_t const result = ParseResult_t::Fail({Cast<i32>(code), msg});
    return result;
}

} // namespace ark
